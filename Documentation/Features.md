# Features

The IDN-Laser-Tester is intended to provide basic functionality to test a IDN setup. It includes the following tools which are accessible from the IDN-Laser-Tester homepage

## Startup

![Screenshot of IDN-Laser-Tester splashscreen displayed after bootup](sources/images/screenshot-splashscreen.png "Splashscreen")

The VirtualBox Appliance is configured to log in and start the IDN-Laser-Tester server automatically - without any user interaction. It then displays a splashscreen with instructions about how to connect to the server from your mobile device and informations like the IP address and the IDN-Laser-Tester status.

By the way: pm2 is used to autostart the application. In case of an unexpected crash it will even immediately restart it.

> **Wrap up** <br>
Try starting the Virtual Machine<br> Can you see the Splashscreen with the Testers IP address and/or hostname?

> **Tip** <br>
If you don't get the splashscreen but the virtual box is stuck at some terminal output you can try the following: <br> - make sure you are connected to a network or wait for 2 minutes if not <br> - click somewhere inside the virtual machine window. Hit Ctrl + C multiple times. When you get something like <b>user@idn-tester:</b>, type `. .profile` and hit enter(yes, it is: dot space dot). You should then see the Splashscreen. <br> - try restarting your virtual machine: Either close the window and chose <b>send the shutdown signal</b> to safely shut down the virtual machine before starting it up again. Alternatively, select the virtual machine's window and click on Machine > ACPI Shutdown. 

## Accessing the web interface from your mobile device

![Screenshot of IDN-Laser-Tester homepage on a desktop webbrowser](sources/images/screenshot-homepage.png "Screenshot of IDN-Laser-Tester homepage on a desktop webbrowser")

To make use of the IDN-Server-Tester connect to the IDN-Laser-Tester follow the instructions from the splash screen:
 - connect your device to the same network as the server
 - open a webbrowser
 - type in the server's IP address displayed on the splashscreen<br>or the displayed hostname followed by ".local" on your iOS/iPadOS/macOS device or computer running a current version of Windows 10

This will lead to the IDN-Laser-Tester homepage from where all the functionality can be accessed:

> **Wrap up** <br>
Try accessing the web application <br> - Make sure the Virtual Machine is up and running and connected to a network <br> - Make sure you are connected to the same network as the Virtual Machine <br> - If connecting using the hostname is not working try the IP address instead. This should always work. <br>

> **Problems** <br> - You <b>should</b> be able to use the browser on the computer you are running VirtualBox on without any problems. However, one test person running Windows 10 had difficulties to do so as the realtime connection to alter scaling/color/... broke quite often. If you are prompted with the corresponding error just reload the page or switch to another device. The status of the realtime connection is displayed in the bottom right corner. The connection is established on page load. <br> - Since the release of iOS 13 there are problems with tapping buttons. If nothing happens just continue clicking.

## Devices: IDN Service Scan

![Screenshot of Devices: IDN Service Scan on a desktop webbrowser](sources/images/screenshot-service_scan.png "Screenshot of Devices: IDN Service Scan on a desktop webbrowser")

The first card on the homepage titled "Devices" leads to a page which is meant for scanning the network for devices capable of the IDN Hello standard which is part of the IDN Discovery Protocol. Be aware that this is currently under development and not yet finished so your devices may lack support for it so far.

By clicking on the Scan button the server scans the network for IDN-Hello capable devices. Afterwards, each device that has responded to the request is asked for the services it provides. The results are then displayed in a grid. The first card combines information about the scan: scan time and the number of available devices (servers). It is followed by a separate card for each discovered server. Each displaying the servername, the category (like 01 for consumer or 02 for switcher), the UnitID and a table listing all provided services with serviceID and service name.

> **Wrap up** <br>
Try scanning the network for IDN-Hello capable devices. <br> - Are you connected to your IDN network? <br> - Are your devices IDN-Hello capable? <br> - Do you at least get the Scan Information card, showing the scan time and the server count (may be 0)?

## Simple Test

![Screenshot of Simple Scan (IDN Tester) on a desktop webbrowser](sources/images/screenshot-tester.png "Screenshot of Simple Scan (IDN Tester) on a desktop webbrowser")

The second card on the homepage titled "Simple Test" leads to page which is meant for basic projector testing. It is actually a IDTF player with the controls are narrowed to just fit the nead for playing some test patterns.

 -  The first card is for setting the destination. It features a dropdown menu listing all services previously discovered using the Devices (IDN Service Scan). This is really the saved response list from the last scan. If you want to rescan, you can either use the Devices (IDN Service Scan) page or click on the little refresh icon in the card's upper right corner. <br><br>Just below are two text boxes labeled "IP*" and "ServiceID". While selecting services from the dropdown menu those boxes are read only but are updated with the selected services information. In case you want to test a non IDN-Hello capable projector you can select "custom" from the dropdown list's bottom. The IP and ServiceID boxes are then unlocked and editable. While the IP box is required, you could leave ServiceID empty. In this case the output will be send to ServiceID 0 which is the default if not changed.
 
 > **Tip: iOS** <br> On iOS devices you will see the rotating selector like in the alarm clock and timer app. Remember to not only scroll through the list but to confirm the selection by tapping on a line. It will then hightlight by changing the color and showing a little checkmark next to the line.

 -  The second box features a dropdown menu as well. It lists a bunch of test patterns. As mentioned before, the tester is actually a IDTF player. As every test pattern is an IDTF file stored on the server this list is actually editable and can be expanded by your own patterns.

 -  The third card features some sliders for R, G, B intensity and scale as well as two checkboxes for mirroring along the X and Y axes and one for Color Shift. Changes to those parameters are passed to the player in realtime and allow live modification to the image. <br> <br> For safety reasons during the projector's first test, the RGB intensity is here always initialized with 0 when pressing the play button.

 -  The fourth card has three player control buttons at the bottom. When pressing the play button, the player is started for 30 seconds. You can then start to increase the RGB values to make the pattern visible. <br> <br> You can stop the player at any time by pressing the stop button.

<div style="
    border-color: #ebccd1;
    margin-bottom: 27px;
    background-color: #fff;
    border: px solid transparent;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0,0,0,0.05);
    box-sizing: border-box;
    display: block;">
<p style="color: #a94442;;
    background-color: #f2dede;
    border-color: #ebccd1;
    line-height: 1.5;
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
    margin: 0 0 13.5px;
    box-sizing: border-box;
    display: block;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;"><b>Saftety Note</b></p>
<div class="panel-body">
<p style="padding: 15px;
    box-sizing: border-box;
    display: block;">The stop button's effect may be delayed up to 1 second. Make sure to always have somebody at the emergency stop button and do not rely on the software button only!</p>
</div>
</div>

> **Wrap up** <br>
Try sending a test pattern to a projector <br> - Can you select a previously discovered projector or adapter from the list? <br> - Can you select different patterns from the list? <br> - Can you start and stop the player? Playing is indicated by progress bar next to the play / stop buttons <br> - Can you modify the Realtime Parameters?

## IDTF Player

![Screenshot of IDTF Player on a desktop webbrowser](sources/images/screenshot-idtf_player.png "Screenshot of IDTF Player on a desktop webbrowser")

The second card on the homepage titled "IDTF Player" leads to page which is almost the same as the Simple Test IDN Tester. This time with some more options.

 -  The first card is exactly the same.

 -  The second card is still for IDTF selection. But this time the list is not filtered and shows every IDTF file. Not just with type test pattern.

 -  New is the card inserted at the third position. It enables the user to set some startup parameters like "scanspeed" and "framerate". "Hold time" sets the duration a single frame file is displayed. The "Loop / Hold forever" checkbox will loop (animation) or hold (single frame) the file until the player is stopped by hand. As the name suggests the startup parameters have to be set before start and do not have any live effect while a player is running.

 -  The realtime parameter section is exactly like before but this time every parameter is passed to the player when starting the player (set values are not initialized with 0 when pressing the start button as the Tester does). This applies to the RGB intensities as well.

 > **Wrap up** <br>
Try to play some animation <br> - Can you send any moving object to the projector? <br> - Can you modifiy the ScanSpeed or Framerate? <br> - Remember that those parameters need to be set before starting the player. <br> - ScanSpeed may not have a huge effect until playing a real show. Unfortunately, at the time beeing there is no show included.

### Multi Projector
You may have wondered what the button with the triple stop icon is for. You already know how to play a file. While you cannot select multiple projectors at once before starting a player you can instead simply start a second (and so on) player by selecting another currently non-busy service from the list and tap the play button again. This way you can add as many players as you have services. You can play any file on any service: all different files or all the same (each will start as you click the play button - so projections are not playing in sync then).

The stop button stops players in the reverse order you have started them. You can also use the button with the triple stop icon to stop all players at once.



> **Explaination** <br> - Starting a player pushes the corresponding service on a stack. The realtime parameter section always controls the uppermost, most recently started (and still active) player only. (From the server's view / globally - not from your browser's or browsertab's view).<br> - If any player on the stack reaches its end of file and is not configured to loop forever, it is removed from the stack and marked as idle.<br> - As the stack and the stop buttons work globally you can also stop testers from the player page and vice versa. This also means that tester and player share the same stack of active players and the realtime controls only have an effect to the most recently started "player" (can be tester or player).<br> - The stack is also shared between every Browser you are accessing the IDN Laser Tester with. The realtime parameter box always controlls the most recently started (and still active) player. No matter by whom and on wich device it was started.

 > **Wrap up** <br>
Try to play multiple files on multiple service (if you have more than one available) to undestand what this stack thing from above means <br> - Tick the <b>Loop / Hold forever</b> Box <br> - Select a service <br> - Start any file <br> - Modify the realtime parameters <br> - Repeat the steps for every service you have to see how it works and which player is affected when changing the realtime parameters. <br> - You may want to stop players as well.


## IDTF Files

![Screenshot of IDTF Files overview on a desktop webbrowser](sources/images/screenshot-idtf_files.png "Screenshot of IDTF Files overview on a desktop webbrowser")

The last card on the homepage titled "IDTF files" leads to an overview of the IDTF files located on the server.

The view is made by reading a file containing a list of the IDTF files on the server featuring information like filename, type and a preview image.



- When hitting the plus button at the top an upload dialog box is displayed at the bottom enabling to upload your own IDTF files. Just leave the thumbail box empty if you don't have a preview image. Click the Save button on the right to upload or the X button on the left to clear the form.

    ![Screenshot of IDTF Files upload dialog on a desktop webbrowser](sources/images/screenshot-idtf_addfile.png "Screenshot of IDTF Files overview on a desktop webbrowser")

- The little edit or pen button toggles the visibility of a little x on every card. Click the x to delete a file.

- The refresh button updates the IDTF File list. You can also refresh the entire page instead.

## Shutdown

To safely shutdown the server (RaspberryPi or VirtualBox) you can use the shuttdown button which is located in the menu bar at the upper right corner on every page. Pressing the button invokes a popup screen asking you to confirm the shutdown request.

![Screenshot of the shutdown confirmation dialog on a desktop webbrowser](sources/images/screenshot-shutdown.png "Screenshot of the shutdown confirmation dialog on a desktop webbrowser")