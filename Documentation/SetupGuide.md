# Setup Guide

For the time being, testing is provided by using the fully preconfigured VirtualBox Appliance (running Ubuntu Server 18.04 LTS) which can be easily imported on your machine.<br>

For detailed instructions on getting installing VirtualBox and getting the Appliance up and running see the [Setup Guide for using the  VBox-Image](./VirtualBox.md).<br>
The Image (VirtualBox Appliance) is available [here](https://uni-bonn.sciebo.de/s/p5s7yvyarnk8HYs) and is currently just around 2GB in size.


> **Info:** For people intending to run the IDN-Laser-Tester server component on a dedicated computer like the RaspberryPi, there may also be a preconfigured image for that. As this requires additional hardware, this option is not available / inteded for testing right now.



### Supported Targets
  - Linux host
  - Linux VM
  - (Windows Subsystem for Linux)
  - Raspberry Pi

If you are running the IDN Laser Tester from a virtual machine or on a Raspberry Pi, you may consider installing either some linux server version like ubuntu server or disable the GUI and to boot into the command line instead after boot. GUI is not required for setting up or running the application so you can save CPU and RAM usage by not using it. Also, a standard GUI linux may include many tools like a webbrowser, some office suite and media players that are not required and would take up disk space.

The installation in the Windows Subsystem for Linux (WSL) is not fully tested but it looks like the IDN-Laser-Tester is compatible with the Ubuntu 18.04 LTS subsystem while Ubuntu 20.04 LTS has some timing issues when playing back IDTF files.

## Installation

> **Hint:** The installation may be easier if executed via SSH so that you can copy and paste commands from your (GUI) host system into the target systems command line.<br>
> **Note:** If you are using a RaspberryPi it is recommended to create a new user and delete the standard `pi` account. Make sure to add the new user to the sudoers group before deleting the `pi` account.<br>
> **Note:** The standard account of the VBox and Raspi Images is `user`.
 
If you want to install the IDN Laser Tester manually follow the steps below

  - NodeJS Installation
    - download Node JS from https://nodejs.org and follow the instrucions from https://github.com/nodejs/help/wiki/Installation
    - or alternatively install via package manager https://nodejs.org/en/download/package-manager/
  - IDN Server Tester Installation
    1) Download or clone the IDN Laser Tester repository
    1) (unpack the zip file)
    1) open the terminal and head to the IDN Laser Tester directory
    1) run `npm install` to download and install the dependencies
    1) navigate to the ´scripts´ directory and run `./compile.sh` to compile the two C applications included in the IDN-Laser-Tester
       - if the compile script fails you may need to install some dependencies first
       - simply follow the instructions in the error message
    1) run `npm start` (from within the directory) to start the server

The steps above are essential for installing, starting and using the IDN Laser Tester. Nonetheless, you can perform some additional steps to simplify interaction and usage. All those steps have been applied to the VirtualBox Appliance as well as the RaspberryPi Image and parts of the documentation rely on them.

## Additional Setup

### mDNS
To enable opening the website by typing the machines hostname instead of the IP address on iOS / iPadOS / macOS or Windows install the avahi-daemon
```
sudo apt-get update
sudo apt-get install avahi-daemon
```
### Standard HTTP Port
By default, the IDN Laser Tester starts a webserver on port 3000. So to open the webpage, you need to enter http://hostname.local:3000 or http://IPAddress:3000 in your webbrowser. To start the server using the standard http Port (80) you would need to run the server with admin permissions - which is not recommended. Instead, you can redirect port 3000 to 80 using `iptables`
```
sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 3000
```
> **Note:** Change `eth0` to the identifier of your IDN network interface. On VirtualBox VMs it might be `enp3s0` instead

To make this rule permanent (after reboot) run the following commands
```
sudo apt-get update
sudo apt-get install iptables-persistent
```
Choose ´yes´ twice to save the current rules for ip_v4 and ip_v6 when prompted.
### Auto Login
If you want to avoid interaction with the host's OS and command line, you may want to configure autologin. This is also recommended for the installation on a RaspberryPi.
Open or create config by typing:
```
sudo nano /etc/systemd/system/getty@tty1.service.d/override.conf
```
and inserting
```
[Service]
ExecStart=
ExecStart=-/sbin/agetty --noissue --autologin myusername %I $TERM
Type=idle
```
where `myusername` is the user you want to be automatically logged in after booting up
### Autostart the Tester
If you want to autostart the IDN Laser Tester on boot, head to the `src` folder inside the IDN Laser Tester directory and run the following commands: 
```
sudo npm install pm2@latest -g
pm2 start index.js
pm2 startup systemd
```
The last line of the block containing `systemd` generates a command which starts the pm2 service on boot (which is responsible for starting the IDN Laser Tester). The generated command will be printed to the command line and needs to be copied and run by hand.

The output should look similar to the one below
```
user@idn-laser-tester:~/IDN-Laser-Tester/src$ pm2 startup systemd
[PM2] Init System found: systemd
user
[PM2] To setup the Startup Script, copy/paste the following command:
sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u user --hp /home/user
```

Copy the last line, starting with `sudo`, paste it into the command line and run the command.
> **Note:** Copy the line from your console output and not the example above as the command is generated specificly for your system!

### Splashscreen
The IDN-Laser-Tester comes with a splashscreen displaying connection instructions and the systems IP address and hostmane. Two templates are located in `<path-to-IDN-Laser-Tester-directory>/src/scripts`. <br>To autostart launching the splashscreen after login:
open `~/.bashrc` by typing
```
nano ~/.bashrc
```
and appending
```
<path-to-IDN-Laser-Tester-directory>/src/scripts/startupVBox.sh
```
(without the angle brackets) to the bottom of the file.
There are two templates for a VirtualBox VM and a RaspberryPi installation in the repository. You may check whether your system's network interface identifier matches the one in the template.
### Link Local Addresses in networks without DHCP Server
Your IDN network may not have a DHCP server connected requiring all devices to have manually assigned static IP addresses. To assign a link local address to the IDN Laser Tester run
```
sudo apt install dhcpcd5
```
to install a DHCP client wich automatically assigns a link local address in case no DHCP server is available. Nevertheless, it does not stop looking for DHCP servers afterwards. So if you change the network or your router takes some time to boot up you will get an DHCP address as soon as it is available.
### Dynamic SSID update

If you use a RaspberryPi but don't want to spend extra money on a display you can use the RaspberryPi's WiFi module to create a WiFi network and use it's SSID to broadcast the RaspberryPi's ethernet IP address

> **Note:** The following only applies to Raspbian and did not work on ubuntu server for RaspberryPi.

Enable the WiFi module. Run
```
sudo rfkill list all
```
Look for the line containing `Wireless LAN`. It's probably
``` 
0: phy0: Wireless LAN
```
Run
```
sudo rfkill unblock 0
```
where `0` is WiFi module's index from the command above. Make sure it is `0` on your device as well.
<br>
Install required software
```
sudo apt install dnsmasq hostapd dhcpcd5
```
Configure DHCP server for WiFi network
```
sudo nano /etc/dhcpcd.conf
```
Append the following lines to dhcpcd.conf to set the IP address the RaspberryPi gets in it's WiFi network. You can change the preset IP address to any valid one
```
interface wlan0 
static ip_address=192.168.20.1/24
```
Run
```
sudo systemctl restart dhcpcd
```
To apply the changes.<br>
Configure DHCP and DNS for the WiFi network and create a backup copy first:
```
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf_alt
sudo nano /etc/dnsmasq.conf
```
Add lines to file (dnsmasq.conf):
```
# Enable DHCP server for WiFi interface only!
interface=wlan0

# Disable DHCP server for Ethernet interface!
no-dhcp-interface=eth0

# IPv4-address range and Lease-Time
dhcp-range=192.168.20.100,192.168.20.200,255.255.255.0,24h

# DNS server for WiFi clients
dhcp-option=option:dns-server,192.168.20.1
```
Run to check and apply configs: 
```
dnsmasq --test -C /etc/dnsmasq.conf
sudo systemctl restart dnsmasq
sudo systemctl status dnsmasq
sudo systemctl enable dnsmasq
```
Configure WiFi network:
```
sudo nano /etc/hostapd/hostapd.conf
```
Add lines to file (hostapd.conf):
```
# Interface
interface=wlan0
#driver=nl80211

# WiFi config
ssid=MyIDNTester
channel=1
hw_mode=g
ieee80211n=1
ieee80211d=1
country_code=DE
wmm_enabled=1

# WiFi encryption settings
auth_algs=1
wpa=2
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
wpa_passphrase=testtest
```

If you want to change the WiFi password, change the `wpa_passphrase` line.
We decided to use a password for security reasons.

Run:
```
sudo hostapd -dd /etc/hostapd/hostapd.conf
sudo nano /etc/default/hostapd
```
Add lines to file
```
RUN_DAEMON=yes
DAEMON_CONF="/etc/hostapd/hostapd.conf"
```
If you are initially setting up the Pi, run
```
sudo systemctl enable hostapd
```
If you have already configured the Pi and just altered the configureation, run 
```
sudo systemctl unmask hostapd
sudo systemctl restart hostapd
```
instead.<br>
The WiFi network is now set up.<br><br>
<br>
To have the ethernet IP address as the SSID of the AP run
```
sudo visudo
```
then insert line at bottom of file
```
user ALL=NOPASSWD: <path-to-IDN-Laser-Tester-directory>/src/scripts/updateSSID.sh
```
(without the angle brackets). The splashscreen  script that you may have configured to run on login will check for the current IP, save it, compares the result with the previously stored IP to detect whether it has changed and to update the ssid line in `hostapd.conf` if so using the separate `updateSSID.sh` script. The line in visudo enables the configuration to be updated without prompting for the password.<br>
> **Reference:** for guide on setting up WiFi network: https://www.elektronik-kompendium.de/sites/raspberry-pi/2002171.htm
> **Note:** If you just want to use the WiFi network but without the dynamic SSID containing the Ethernet IP, modify the splashscreen template to not call the `updateSSID.sh` script.<br>
> **Note:** When running into problems you may want to check the interface identifiers in the splashscreen template script.

### Shutdown button in Web application
To enable the shutdown button in the upper right corner on any page of the IDN-Laser-Tester run 
```
sudo visudo
```
and append the following line to allow executing the shutdown command without the need to enter a su password:
```
user ALL=NOPASSWD: /sbin/shutdown
```
where `user` has to be replaced with the user that executes the IDN-Laser-Tester

### Disable long timeout on shutdown or reboot 
When trying to shutdown or reboot your device you may be faced with a timeout delaying the reboot or shutdown. To lower the timeout time run
```
sudo nano /etc/systemd/system.conf
```
and change the timeout in line
```
DefaultTimeoutStartSec=10s
DefaultTimeoutStopSec=10s
```
and remove leading `#`<br>
Finally, apply the changes by running
```
sudo  systemctl daemon-reload
```

### RaspberryPi: Usage with a HDMI display
If you use a display that gets its power from the raspi's USB ports you may want to enforce enabling the HDMI port all the time.
Run
```
sudo nano /boot/config.txt
```
and uncomment (remove the leading #)
```
hdmi_force_hotplug=1
hdmi_drive=2
```
The RaspberryPi seems to check whether a display is connected to the HDMI just after connecting it to a power source. The HDMI port won't be activated if it is unused. As the display gets its power from the RaspberryPi's USB ports it may start up with a little delay - and miss the time the raspi is looking for whether the port is connected.

### RaspberryPi: Disable under-voltage warning
The RaspberryPi is pretty finicky concerning the power source's voltage leading to repeated warnings printed to the command line. Those may survive the `clear` command and mess up the splashscreen. The warning is legitimate but in case you know what you are doing or ignoring it anyway you can disable it after all.
Run
```
sudo nano /boot/config.txt
```
and append the following to the end of the file:
```
# Disable under-voltage warning
avoid_warnings=1
```
