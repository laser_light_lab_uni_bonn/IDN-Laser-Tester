# Background

What is the IDN-Laser-Tester's purpose?

## Analogue past

In the analogue past, laser projectors were controlled using 25-Pin wires, originally used for hooking up printers. Those were handy because they had just a sufficient amount of strands and were already in the market. Using this established type of cable and connectors was much cheaper than developing a completely new standard.

Every parameter like color and scanner position was individually controlled by the volage applied to the corresponding strand. This is a rather straightforward imlementation.

But analog control comes at a cost: Long cables - all the more with the huge amount of strands - are prone to interference. Besides, every linkeage  can impair the signal's quality.

Finally, there is another big constraint: As almost all strands of the DB-25 cable are used, you can only control one projector at a time using one single cable. So for every projector displaying a different picture, you need to have another cable reaching from the operator's workspace to the projector's site.

## Digital present age

In July 2015, the [ILDA]("https://ilda.com") released the first revision of the [ILDA Digital Network (IDN) Stream Specification]("https://www.ilda.com/resources/StandardsDocs/ILDA_IDN-Stream_rev001.pdf") which specifies how ILDA data can be transmitted over conventional IP based networks. This allows to send numerous individual streams to individual projectors over one single network cable. Eventually, this can also lead to obeviating the need for numerous highly prized ILDA DB-25 computer interfaces - provided that your software is IDN capable.

## Difficulties ensuing the IDN

Troubleshooting the analogue ILDA DB-25 connections was pretty straightforward: The signal applied to one end of a wire should be on the other end as well.
As IP networks allow multiple connections and streams over one cable, things are a little more compilcated in the IDN.

You have to setup the IDN components properly. And you have to make sure the IP network is configured properly as well.
And as you can send multiple streams to multiple projectors from one computer, plugging only one ethernet cable into the computer, troubleshooting is not as easy as just following the wire.

## Testing

For the analogue world, there are tools available that are actually small handheld boxes featuring a DB-25 port. They can be hooked up to a projector and will output a test image: e.g. a RGBW colored square. This does not require much effort but is just sufficient for testing a projector's main components: scanner and laser emitter.

The IDN-Laser-Tester is supposed to act as an IDN equivalent and is adapted to fit the IDN's needs. It features the basic functionality to send test images to a projector. Beyond, it is able to scan the network for IDN Hello capable devices and list them.

For a complete list of features see [Features](/Documentation/Features.md)