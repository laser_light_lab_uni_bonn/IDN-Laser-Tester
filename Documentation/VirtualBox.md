# Run the server within a VirtualBox VM

If you like IDN Laser Tester you may consider buying a RaspberryPi to run the server on. For the purpose of testing the application the server will run inside a VirtualBox virtual machine hosted on your computer. It is entirely free to use and does not require any additional hardware (like a RaspberryPi).

## Getting started

### Prerequisites
  - a computer with a CPU capable of virtualization (this should apply to most computers not older than ~10 years)
  - at least 1GB of free RAM
  - at least 5GB of free disk space
  - a network interface (LAN or WiFi connected to the IDN Network)
  - a working VirtualBox installation

#### Virtual Box Installation

To install the free software VirtualBox head to https://www.virtualbox.org and download the installer for your operating system. VirtualBox is available for many versions of Windows, macOS and Linux.

After the download has finished run the installer and follow the instructions on screen.

##### macOS

1) When asked for the permission to "run a program to determine if the software can be installed", click continue
1) As the standard install location is fine, click "Install"
1) When promted, enter your admin password
1) Wait for the software being installed.
1) Click on "Open Security Preferences" when the warning "System Extension Blocked" appears. (Alternatively open macOS System Preferences > Security & Privacy.) The Installation may fail at this exact moment. Just ignore the installation window)
1) At the bottom of the window below "Allow Apps from" should appear a message like "System software from developer "Oracle America, Inc." was blocked from loading."

![Screenshot of Security pane in Systempreferences on macOS during VirtualBox installation](sources/images/screenshot-macOS_allow_Extension.png "Allow Extension during VirtualBox installation on macOS")

1) Click on allow. (Depending on your macOS version you may need to allow modifying the settings by clicking on the lock icon at the bottom left corner and entering an admin password to enable the "Allow" button) If there are more than one blocked extensions, make sure to click "Allow" just next to the VirtualBox / Oracle warning. Other extensions are not part of VirtualBox. Allow those on your own risk or skip them for now)
1) Close System Preferences
1) Restart the VirtualBox installer and follow the steps above. This time the installation should succeed without any warnings.


Depending on your macOS Version or VirtualBox installer, the installation may fail just before the status bar reaches 100% without any warning.
In this case:
  1) Open macOS System Preferences > Security & Privacy
  1) Select the General tab (may be the default when opening for the first time)
  1) At the bottom of the window below "Allow Apps from" should appear a message like "System software from developer "Oracle America, Inc." was blocked from loading."
1) Click on allow. (Depending on your macOS version you may need to allow modifying the settings by clicking on the lock icon at the bottom left corner and entering an admin password to enable the "Allow" button) If there are more than one blocked extensions, make sure to click "Allow" just next to the VirtualBox / Oracle warning. Other extensions are not part of VirtualBox. Allow those on your own risk)
1) Close System Preferences
1) Restart the VirtualBox installer and follow the steps above. This time the installation should succeed without any warnings.

If the installation failed and you do not see the message in the Security pane, quit System Preferences by pressing CMD+Q or via the macOS menu bar and reopen System Preferences.

You find step by step screenshots that may help you [here](sources/images/VirtualBox-Windows)

##### Windows

1) Click next when the installer's welcome screen appears
1) As there is no need to change any settings on the Custom Setup page, click next again
1) It is recommended to create a start menu entry and register file associations. If you want to have shortcuts on your desktop or the Quick Launch Bar, click next. Otherwise untick the respective boxes.
1) The installation will temporarily disconnect you from connected networks. If you have any important tasks requiring the network connection running, make sure to finish those before proceeding by clicking yes.
1) Click Install to start the installation
1) Allow the installation by clicking yes (may require admin password input)
1) IMPORTANT! When prompted allow the installation of VirtualBox drivers (e.g. USB and network) by clicking on install. Otherwise the IDN Laser Tester won't work!
1) Finish the installation.
1) Just to be safe everything will work as expected you may restart your computer after the installation has finished.

You find step by step screenshots that may help you [here](sources/images/VirtualBox-Windows)

#### Opening the VirtualBox Appliance

Instead of having to set up a virtual machine, installing an operating system and the IDN Laser Tester manually, you will import a preconfigured virtual machine that is ready to go.

To do so
  1) Download the provided .ova file from [here](https://uni-bonn.sciebo.de/s/p5s7yvyarnk8HYs) (currently just around 2GB in size)
  1) Import the virtual machine by <br>
    a) clicking File > Import Appliance <br>
    a) or simply double click on the ova file
  1) Follow the wizard on screen to import the Virtual Machine Appliance

  As the Appliance was created on a machine different than yours you have to adjust the virtual machine's network adapter settings:
  1) Open Virtual Box
  1) Select the imported virtual machine by clicking on the corresponding item in the list on the left (during testing, one VirtualBox installation on one test computer running Windows 10 required a right click for selection)
  1) Click on the gear icon above the selected virtual machine's details on the righthand area of the window
  1) Head to the Network tab
  1) Select "Adapter 1"
  1) Make sure the option "activated" is enabled
  1) Make sure the option "connected to" is set to "network bridge"
  1) Next to "name", select the IDN connected network interface from the dropdown menu. It is probably either your WiFi or Ethernet interface.
  1) Close the settings window and start the virtual machine by clicking on the green arrow icon named "run"

## Notes

![Screenshot of IDN-Laser-Tester mouse pointer integration warning on Windows](sources/images/screenshot-mouse_capture.png "Mouse Capture dialog")

> **Note: Mouse pointer integration** <br> The Virtual Box window should not but may catch your mouse even if the Virtual Machine has no graphical user interface and hence no mouse. To free the cursor, press the left CMD key on macOS or the right CTRL key on Windows. <br> There is a message box on every VM start which explains the behaviour and mentions the exact key(s) to press. Please remember the key or try to escape by openign the task manager or switching between apps with ALT+TAB or WINDOWS+TAB.

![Screenshot of IDN-Laser-Tester shutdown dialog when closing window](sources/images/screenshot-shutdownVM.png "Shutdown VM")

> **Note: Shutdown** <br> You can shutdown the Virtual Machine by closing the VM's window. It will then show a prompt where you have to choose what to do with the machine.<br> - You can *save the machine state*. On the next start it will recover from that without the need to boot. This is handy if your virtualized operating system needs a lot of time to boot. The problem here is that the splashscreen may disappear after recovering the saved state as it is loading on boot and some status messages may interrupt the splashscreen. <br> - You can *send the shutdown signal* **(recommended)**. This will safely shutdown the VM. On the next boot everything should be as expected. <br> - You can *power off the machine* (force shutdown). This will instantly kill the VM and is not recommended at all because it may lead to data loss or unexpected behaviour on the next boot. Only use this if the VM froze and is won't shutdown safely.