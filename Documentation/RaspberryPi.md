# Run the server on a RaspberryPi

If you like to use the tester on a RaspberryPi, the easiest way is to clone a preconfigured image on a microSD card.

The image will be available [here](https://uni-bonn.sciebo.de/s/Ym9FRI5vkcv4MDv)

## Getting started

### Prerequisites
  - a RaspberryPi 3
  - a at least 4GB microSD Card
  - a computer to download and clone the image to the microSD card

### Step by Step guide

1. Download the RaspberryPi IDN-Laser-Tester image
1. Clone the image to the microSD card:

#### macOS

1. Download balenaEtcher from [www.balena.io](https://www.balena.io/etcher/)
1. Mount the balenaEtcher .dmg file and run the balenaEtcher application
1. Select image: Select the IDN-Laser-Tester Image
1. Select drive: Select the microSD card. <br>
Make sure it really is the microSD card as all data on the selected drive will be overwritten!
1. Flash!
1. Enter your password. The low level access to the microSD card's file system requires admin privileges.

> If you are running macOS 10.15 Catalina you may get asked whether you want to allow balenaEtcher to access your Documents or Downloads directory. Click okay as the tool needs to access the IDN-Laser-Tester image from one of the directories.

#### Windows

1. Download balenaEtcher from [www.balena.io](https://www.balena.io/etcher/)
1. Install the Tool and run it afterwards
1. Select image: Select the IDN-Laser-Tester Image
1. Select drive: Select the microSD card. <br>
Make sure it really is the microSD card as all data on the selected drive will be overwritten!
1. Flash!
1. Enter your password. The low level access to the microSD card's file system requires admin privileges.

After successfully flashing the image to the microSD card

3. eject the microSD card
1. plug the microSD card into the RaspberryPi's slot
1. plug in the power cable to boot the RaspberryPi

### Connect to the RaspberryPi

If you can't connect using the hostname (idn-tester-raspi.local) you will either have to
- connect a display (little screen designed for RaspberryPi or standard HDMI screen)
- or to scan the network to get the RaspberryPi's IP address using a network scanner mobile app or the DHCP servers / WiFi routers configuration interface
- scan for WiFi networks and look for the "IDN@AAA.BBB.CCC.DDD/EE" Network wich is created by your Raspi adn where the AAA.BBB.CCC.DDD is the Raspis Ethernet IP address
- or to directly connect to the Raspi's WiFi network. The initial password is "testtest" but can be changed to any valid WPA2 key. While connected to the Raspi's WiFi network the Application can be accessed by visiting the Raspi's static http://192.168.20.1 in your browser.