# Disclaimer

We included a few IDTF files so you have anything to test the player with.

The IDN-Laser-Tester currently includes a few IDTF files from the following repositories
- [Arroquw](https://github.com/Arroquw/Project-Laser)
- [jheidel](https://github.com/jheidel/ece4760-lab5/tree/master/datafiles)

The respecting references can be found in the `src/public/json/*.json` file and are displayed in the IDTF section of the web interface.