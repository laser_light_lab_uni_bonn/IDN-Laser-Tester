window.onload = function () {
  //helloScan();
  refreshIDNServerList();
  refreshIDTFFileList(0);
};

function startPlayer() {

  if (checkFormValidity()) {
    //showToast("Please check your inputs", 3000, red)
    return
  }

  let idtfSelector = document.getElementById("idtfSelector");
  let serviceID_input = document.getElementById("serviceID_input");

  let initialScale = document.getElementById("scaleSlider").value;

  let ip = document.getElementById("ip_input").value;
  let fileName = idtfSelector.options[idtfSelector.selectedIndex].getAttribute("fileName");
  let scanSpeed = document.getElementById("scanSpeed_input").value;
  let fps = document.getElementById("frameRate_input").value;
  let holdTime = document.getElementById("holdTime_input").value;
  let scale = initialScale ? initialScale : 100;
  let inf_loop = document.getElementById("infLoopBox").checked;

  if (ip != null && fileName != null) {
    let request = {};

    request.ip = ip;
    request.fileName = fileName;
    request.scanSpeed = scanSpeed ? scanSpeed : 30000;
    request.fps = fps ? fps : 30;
    request.holdTime = holdTime ? holdTime : 5;
    request.scale = scale / 100.0;
    request.serviceid = serviceID_input.value;
    request.infloop = inf_loop ? 1 : 0;
    request.initr = document.getElementById("rSlider").value;
    request.initg = document.getElementById("gSlider").value;
    request.initb = document.getElementById("bSlider").value;
    request.scale = document.getElementById("scaleSlider").value;
    request.sft = document.getElementById("sftSlider").value;
    if (document.getElementById("mirrorXBox").checked) {
      // idn-C-idtfPlayer expects flag only - without value
      request.mx = null;
    }
    if (document.getElementById("mirrorYBox").checked) {
      // idn-C-idtfPlayer expects flag only - without value
      request.my = null;
    }

    post('/api/player', request)
      .catch(error => console.error(error));

    function post(url, data) {
      displayMDLProgressInterminate();
      return fetch(url, {
        body: JSON.stringify(data),
        headers: {
          'content-type': 'application/json'
        },
        method: 'POST'
      })
        .then(response => interpretPlayerResponse(response));
    }
  }
}