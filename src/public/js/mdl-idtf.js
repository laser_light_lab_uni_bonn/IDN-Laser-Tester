let idtf_types = [];
let idtfSelector = document.getElementById("idtfSelector");
let thumbnailSelector = document.getElementById("thumbnailSelector");
let typeSelector = document.getElementById("typeSelector");

idtfSelector.oninput = function () {
  console.log("idtfSelector.oninput()")
  let filename = this.value.split('\\');
  document.getElementById("idtfFileName_input").value = filename[filename.length-1];
}

thumbnailSelector.oninput = function () {
  console.log("thumbnailSelector.oninput()")
  let filename = this.value.split('\\');
  document.getElementById("thumbnailFileName_input").value = filename[filename.length-1];
}

window.onload = function () {
  refreshIDTFGrid();
};

function refreshIDTFGrid() {
  let response_grid = document.getElementById("response-grid");
  console.log("Refresh()2s");

  response = fetch('/api/basicIDTFFiles')
    .then(function (response) {
      return response.json();
    })
    .then(function (response) {
      let string = JSON.stringify(response);
      let parsed = JSON.parse(string);
      return (parsed);
    })
    .then(function (file) {
      idtf_types = file.types;
      refreshTypeList();

      deleteChildNodes(response_grid);

      for (let j = 0; j < file.files.length; j++) {

        let card = document.createElement("div");
        card.setAttribute("class", "mdl-card mdl-cell mdl-cell--2-col mdl-shadow--2dp");

        let card_menu = document.createElement("div");
        card_menu.setAttribute("class", "mdl-card__menu");

        let card_menu_action_button = document.createElement("button");
        card_menu_action_button.setAttribute("class", "mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect selector_deleteButton");
        card_menu_action_button.setAttribute("onclick", "deleteButtonAction(this)");
        card_menu_action_button.hidden = true;
        let delete_icon = document.createElement("i");
        delete_icon.setAttribute("class", "material-icons white-icons");
        delete_icon.innerText = "close";
        card_menu_action_button.appendChild(delete_icon);
        card_menu.appendChild(card_menu_action_button);

        card.appendChild(card_menu);

        let figure = document.createElement("figure");
        figure.setAttribute("class", "mdl-card__media");
        let img = document.createElement("img");
        img.src = "/idtfThumbnails/" + file.files[j].Thumbnail;
        figure.appendChild(img);
        card.appendChild(figure);

        let card_title = document.createElement("div");
        card_title.setAttribute("class", "mdl-card__title");
        let h1 = document.createElement("h1");
        h1.setAttribute("class", "mdl-card__title-text");
        h1.innerHTML = file.files[j].Name;
        card_title.appendChild(h1);
        card.appendChild(card_title);

        let supp_text = document.createElement("div");
        supp_text.setAttribute("class", "mdl-card__supporting-text");
        let p1 = document.createElement("span");
        p1.setAttribute("class", "selector_IDTFFileName");
        p1.innerHTML = "File: " + file.files[j].FileName;
        let p2 = document.createElement("span");
        p2.innerHTML = "Type: " + file.files[j].Type;

        supp_text.appendChild(p1);
        supp_text.appendChild(p2);

        if (file.files[j].Reference != undefined) {
          let p3 = document.createElement("span");
          p3.innerHTML = "Reference: " + file.files[j].Reference;
          supp_text.appendChild(p3);
        }

        card.appendChild(supp_text);
        response_grid.appendChild(card);
      }
    })
}

function showForm() {
  let formular_card = document.getElementById("formular-card");
  formular_card.hidden = false;
  formular_card.scrollIntoView();
}

function refreshTypeList() {
  deleteChildNodes(typeSelector);

  if (idtf_types.length < 1) {
    showToast("There are no Types specified in basic-idtf.json", 2000, red_c);
  } else {
    for (let i = 0; i < idtf_types.length; i++) {
      let option = document.createElement("option");
      option.setAttribute("class", "idtfTypeOption");
      option.innerHTML = idtf_types[i];

      typeSelector.appendChild(option);
    }
  }
}

function submitForm() {

  if (idtfSelector.files.length == 1) {
    let f_data = new FormData();

    f_data.append("displayName", document.getElementById("dispName_input").value);
    f_data.append("type", typeSelector.options[typeSelector.selectedIndex].text);
    f_data.append("idtfFileName", document.getElementById("idtfFileName_input").value);
    f_data.append("idtf", idtfSelector.files[0], document.getElementById("idtfFileName_input").value);
    if (thumbnailSelector.files.length > 0) {
      f_data.append("thumbnailFileName", document.getElementById("thumbnailFileName_input").value);
      f_data.append("thumbnail", thumbnailSelector.files[0], document.getElementById("thumbnailFileName_input").value);
    }

    post('/api/upload', f_data)
      .catch(error => console.error(error));

    function post(url, data) {
      return fetch(url, {
        method: 'POST',
        contentType: 'multipart/form-data',
        boundary: "boundary",
        processData: false,
        body: data
      })
        .then(function (query) {
          if (query.ok === true) {
            showToast(query.statusText, 2000);
            refreshIDTFGrid();
            clearForm();

          } else {
            showToast(query.statusText, 3000);
          }
        })
    }
  } else {
    showToast("No IDTF file selected!", 3000);
  }



}

function clearForm() {
  document.getElementById("dispName_input").value = "";
  document.getElementById("idtfFileName_input").value = "(select IDTF)";
  idtfSelector.files[0] = null;
  thumbnailSelector.files[0] = null;
  document.getElementById("thumbnailFileName_input").value = "(select Thumbnail)";
}

function deleteButtonAction(element) {

  let request = {};
  request.idtfFileName = element.closest(".mdl-card").getElementsByClassName("selector_IDTFFileName")[0].innerText.substring(6);

  post('/api/deleteIDTF', request)
    .catch(error => console.error(error));

  function post(url, data) {
    return fetch(url, {
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST'
    })
      .then(function (query) {
        if (query.ok === true) {
          showToast(query.statusText, 2000);
          refreshIDTFGrid();
          clearForm();

        } else {
          showToast(query.statusText, 3000);
        }
      })
  }
}

function toggleDeleteButtons() {
  let delButtonArr = document.getElementsByClassName("selector_deleteButton");
  if (delButtonArr.length > 0) {
    let areHidden = delButtonArr[0].hidden;

    for (i = 0; i < delButtonArr.length; i++) {
      delButtonArr[i].hidden = !areHidden;
    }
  }
}