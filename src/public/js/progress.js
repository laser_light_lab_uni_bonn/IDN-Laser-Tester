function displayProgress() {
  let progress = document.getElementById('progress');
  progress.removeAttribute('hidden');
}

function hideProgess() {
  let progress = document.getElementById('progress');
  progress.setAttribute('hidden', true);
}