// Buttons
const reloadButton = document.getElementById('reloadButton');
const scanButton = document.getElementById('scanButton');

// Button Event Listeners
scanButton.addEventListener('click', event => {
  displayMDLProgressInterminate();
  query = fetch('/api/runServerlist')
    .then(function (query) {
      if (query.ok === true) {
        refreshIDNServerList();
      } else {
        //TODO: Snackbar
        alert("Error scanning for IDN devices.");
      }
      hideMDLProgressInterminate();
    })
});

reloadButton.addEventListener('click', event => {
  refreshIDNServerList();
});

function refreshIDNServerList() {
  response = fetch('/api/serverlist')
    .then(function (response) {
      return response.json();
    })
    .then(function (response) {
      let string = JSON.stringify(response);
      let parsed = JSON.parse(string);
      return (parsed);
    })
    .then(function (file) {
      let time = file.Time;

      let responseContainer = document.getElementById("response-container");              //encapsulate response in containerNode div
      if (responseContainer !== null && responseContainer.hasChildNodes()) {
        //checks whether serverlist is already displayed. Deltion if true.
        deleteChildNodes(responseContainer);
      }

      let responseGrid = document.createElement("div");
      responseGrid.setAttribute("class", "mdl-grid");

      let infoCard = document.createElement("div");
      infoCard.setAttribute("class", "mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col-phone");

      let infoCardTitle = document.createElement("div");
      infoCardTitle.setAttribute("class", "mdl-card__title");

      let infoCardTitleH2 = document.createElement("h2");
      infoCardTitleH2.setAttribute("class", "mdl-card__title-text")
      infoCardTitleH2.innerHTML = "Scan Information"
      infoCardTitle.appendChild(infoCardTitleH2);
      infoCard.appendChild(infoCardTitle);

      let supportingText = document.createElement("div");
      supportingText.setAttribute("class", "mdl-card__supporting-text mdl-card--expand");

      let spanTime = document.createElement("span");
      spanTime.textContent = "Time: " + time;
      supportingText.appendChild(spanTime);
      let spanCount = document.createElement("span");
      spanCount.textContent = "ServerCount: " + file.ServerList.length;
      supportingText.appendChild(spanCount);

      infoCard.appendChild(supportingText);

      responseGrid.appendChild(infoCard);




      //#####

      //handle serverlist IDN device responses
      if (file.ServerList.length > 0) {
        for (let i = 0; i < file.ServerList.length; i++) {

          let server = file.ServerList[i];

          let Card = document.createElement("div");
          Card.setAttribute("class", "mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col-phone");

          let CardTitle = document.createElement("div");
          CardTitle.setAttribute("class", "mdl-card__title");

          let CardTitleH2 = document.createElement("h2");
          CardTitleH2.setAttribute("class", "mdl-card__title-text")
          CardTitleH2.innerHTML = server.HostName;
          CardTitle.appendChild(CardTitleH2);
          Card.appendChild(CardTitle);

          let supportingText = document.createElement("div");
          supportingText.setAttribute("class", "mdl-card__supporting-text mdl-card--expand");

          for (var key in server) {
            if (server.hasOwnProperty(key) && ((key != "HostName") && (key != "ProvidedServices"))) {
              let span = document.createElement("span");
              let info = "";

              if (key === "Category") {
                switch (server[key]) {
                  case "01":
                    info = " - Consumer";
                    break;
                  case "02":
                    info = " - Switcher";
                    break;
                  default:
                    info = " - unknown";
                }
              }
              span.textContent = key + ": " + server[key] + info;
              supportingText.appendChild(span);
            }
          }

          let services = file.ServerList[i].ProvidedServices;
          if (services.length == 0) {
            let span1 = document.createElement("span");
            span1.textContent = "(No Services)";
            supportingText.appendChild(span1);
          } else {
            //TODO: Spacer
            let table = document.createElement("table");
            table.setAttribute("class", "mdl-data-table mdl-js-data-table serviceList-table");
            let thead = document.createElement("thead");
            let thead_tr = document.createElement("tr");
            let thead_tr_th1 = document.createElement("th");
            thead_tr_th1.innerHTML = "ID";
            let thead_tr_th2 = document.createElement("th");
            thead_tr_th2.setAttribute("class", "mdl-data-table__cell--non-numeric");
            thead_tr_th2.innerHTML = "Service Name";
            thead_tr.appendChild(thead_tr_th1);
            thead_tr.appendChild(thead_tr_th2);
            thead.appendChild(thead_tr);
            table.appendChild(thead);

            let tbody = document.createElement("tbody");
            for (j = 0; j < services.length; j++) {
              let tr = document.createElement("tr");

              let td_id = document.createElement("td");
              td_id.innerHTML = services[j].ServiceID;
              let td_name = document.createElement("td");
              td_name.setAttribute("class", "mdl-data-table__cell--non-numeric");
              td_name.innerHTML = services[j].ServiceName

              tr.appendChild(td_id);
              tr.appendChild(td_name);
              tbody.appendChild(tr);
            }
            table.appendChild(tbody);
            supportingText.appendChild(table);
          }

          Card.appendChild(supportingText);

          responseGrid.appendChild(Card);
        }
      }
      responseContainer.appendChild(responseGrid);
    })
}