window.onload = function () {
  //helloScan();
  refreshIDNServerList();
  refreshIDTFFileList(1);
};

function startPlayer() {

  if (checkFormValidity()) {
    return;
  }

  document.getElementById("rSlider").value = 0;
  document.getElementById("gSlider").value = 0;
  document.getElementById("bSlider").value = 0;

  let idtfSelector = document.getElementById("idtfSelector");
  let serviceID_input = document.getElementById("serviceID_input");

  let ip = document.getElementById("ip_input").value;
  let fileName = idtfSelector.options[idtfSelector.selectedIndex].getAttribute("fileName");
  let scanSpeed = 30000;
  let fps = 30;
  let holdTime = 30;

  if (ip != null && fileName != null) {
    let request = {};

    request.ip = ip;
    request.fileName = fileName;
    request.scanSpeed = scanSpeed;
    request.fps = fps;
    request.holdTime = holdTime;
    request.serviceid = serviceID_input.value;
    request.initr = 0;
    request.initg = 0;
    request.initb = 0;
    request.scale = document.getElementById("scaleSlider").value;
    request.sft = document.getElementById("sftSlider").value;
    
    // Reset check boxes
    document.getElementById("mirrorXBox").checked = false;
    document.getElementById("mirrorYBox").checked = false;
    // checked attribute is made visible through mdl class "is-checked"
    document.querySelectorAll('[for="mirrorXBox"]')[0].classList.remove("is-checked");
    document.querySelectorAll('[for="mirrorYBox"]')[0].classList.remove("is-checked");

    post('/api/player', request)
      .catch(error => console.error(error));

    function post(url, data) {
      displayMDLProgressInterminate();
      return fetch(url, {
        body: JSON.stringify(data),
        headers: {
          'content-type': 'application/json'
        },
        method: 'POST'
      })
        .then(response => interpretPlayerResponse(response));
    }
  }
}