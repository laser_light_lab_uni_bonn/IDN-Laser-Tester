let serverlistContainer = document.getElementById("serverlistSelectionCotainer");
let serverlistSelector = document.getElementById("serverlistSelector");
let serviceInformationContainer = document.getElementById("serviceInformationContainer");
let ip_input = document.getElementById("ip_input");
let serviceID_input = document.getElementById("serviceID_input");
let idtfSelectorContainer = document.getElementById("idtfSelectorContainer");
let idtfSelector = document.getElementById("idtfSelector");

window.onload = function () {
  query = fetch('/api/runServerlist')
    .then(function (query) {
      if (query.ok === true) {
        refreshIDNServerList();
      } else {
        alert("Error scanning IDN devices.");
      }
    })
  refreshIDTFFileList();
};

serverlistSelector.addEventListener('click', event => {
  //console.log(serverlistSelector.options[serverlistSelector.selectedIndex].text);
  //console.log(serverlistSelector.options[serverlistSelector.selectedIndex].getAttribute('ip'));
  let selected = serverlistSelector.options[serverlistSelector.selectedIndex];

  if (selected.getAttribute('value') == "custom") {
    ip_input.disabled = false;
    //ip_input.focus();
    serviceID_input.disabled = false;
  } else {
    //console.log(selected.getAttribute("ip"));
    ip_input.value = selected.getAttribute("ip");
    ip_input.disabled = true;
    serviceID_input.value = selected.getAttribute("serviceid");
    serviceID_input.disabled = true;
  }
  deleteContent(serviceInformationContainer);
  //updateInformation();
});

idtfSelector.addEventListener('click', event => {
  updateThumbnail();
})

function updateThumbnail(selectedIndex) {
  let selected = idtfSelector.options[idtfSelector.selectedIndex];
  let thumbnail = document.getElementById("idtfThumbnail");
  thumbnail.setAttribute("src", "/idtfThumbnails/" + selected.getAttribute("thumbnail"));
}

function updateInformation() {
  let child = serviceInformationContainer.childNodes;
  console.log(serviceInformationContainer.childNodes.length);
}

function deleteContent(node) {
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
}

function refreshIDTFFileList() {
  response = fetch('/api/basicIDTFFiles')
    .then(function (response) {
      return response.json();
    })
    .then(function (response) {
      let string = JSON.stringify(response);
      let typeArray = getTypes(string);
      let parsed = JSON.parse(string);
      return ([parsed, typeArray]);
    })
    .then(function (passed) {

      let file = passed[0];
      let types = passed[1];

      if (types.length < 1) {
        console.log("No Data");
      } else {
        for (let i=0; i<types.length; i++) {
          let optgroup = document.createElement("optgroup");
          optgroup.setAttribute("label", types[i]);
          
          for (let j=0; j<file.files.length; j++) {
            if (file.files[j].Type === types[i]) {
              let option = document.createElement("option");
              option.setAttribute("class", "idtfOption");
              option.setAttribute("type", types[j]);
              option.setAttribute("fileName", file.files[j].FileName);
              option.setAttribute("thumbnail", file.files[j].Thumbnail);
              option.innerHTML = file.files[j].Name;

              optgroup.appendChild(option);
            }
          }

          idtfSelector.appendChild(optgroup);
        }
      }
      
      updateThumbnail();
    })
}

function getTypes(input) {
  let types = [];

  _ = JSON.parse(input, function (key, value) {
    if (key === "Type") {
      if (!(types.includes(value))) {
        types.push(value);
      }
    }
  })
  return types;
}

function refreshIDNServerList() {
  response = fetch('/api/serverlist')
    .then(function (response) {
      return response.json();
    })
    .then(function (response) {
      let string = JSON.stringify(response);
      let parsed = JSON.parse(string);
      return (parsed);
    })
    .then(function (file) {
      //let time = file.Time;

      let serverlistResponsesContainer = document.getElementById("serverlistResponse");

      deleteContent(serverlistResponsesContainer);

      //handle serverlist IDN device responses
      if (file.ServerList.length > 0) {
        for (let i = 0; i < file.ServerList.length; i++) {
          let server = file.ServerList[i];

          if (server.ProvidedServices.length != 0) {
            for (let j = 0; j < server.ProvidedServices.length; j++) {
              let option = document.createElement("option");
              option.innerHTML = server.ProvidedServices[j].ServiceID + "-" + server.ProvidedServices[j].ServiceName;
              option.setAttribute("ip", server.ServerAddress);
              option.setAttribute("serviceID", server.ProvidedServices[j].ServiceID);
              option.setAttribute("value", "selection");
              serverlistResponsesContainer.appendChild(option);
            }
          }
        }
      } else {
        let option = document.createElement("option");
        option.innerHTML = "- No Results -"
        serverlistResponsesContainer.appendChild(option);
      }

      //console.log("Service Count: " + serverlistResponsesContainer.childElementCount);
      deleteContent(serviceInformationContainer);
      let span = document.createElement("span");
      span.innerHTML = "Available HelloScan Services: " + serverlistResponsesContainer.childElementCount;
      serviceInformationContainer.insertBefore(span, serviceInformationContainer.childNodes[1]);
    })
}

  //https://stackoverflow.com/questions/34458805/make-dynamic-square-div-for-an-memory-game - www139


  //PLAYER

  let scaleSlider = document.getElementById("scaleSlider");
  let rSlider = document.getElementById("rSlider");
  let gSlider = document.getElementById("gSlider");
  let bSlider = document.getElementById("bSlider");
  let mirrorXBox = document.getElementById("mirrorXBox");
  let mirrorYBox = document.getElementById("mirrorYBox");
  
  let scaleValueLabel = document.getElementById("scaleValueLabel");
  let rValueLabel = document.getElementById("rValueLabel");
  let gValueLabel = document.getElementById("gValueLabel");
  let bValueLabel = document.getElementById("bValueLabel");
  //let stopButton = document.getElementById('stopPlayerButton');
  
  scaleValueLabel.innerHTML = scaleSlider.value + " %";
  rValueLabel.innerHTML = rSlider.value + " %";
  gValueLabel.innerHTML = gSlider.value + " %";
  bValueLabel.innerHTML = bSlider.value + " %";
  
  // Update the current slider value (each time you drag the slider handle)
  scaleSlider.oninput = function() {
    scaleValueLabel.innerHTML = this.value + " %";
    sendMsg("rtm" + this.value + " s");
  }
  rSlider.oninput = function() {
    rValueLabel.innerHTML = this.value + " %";
    sendMsg("rtm" + this.value + " r");
  } 
  gSlider.oninput = function() {
    gValueLabel.innerHTML = this.value + " %";
    sendMsg("rtm" + this.value + " g");
  } 
  bSlider.oninput = function() {
    bValueLabel.innerHTML = this.value + " %";
    sendMsg("rtm" + this.value + " b");
  } 
  mirrorXBox.oninput = function() {
    sendMsg("rtm" + (this.checked ? 1 : 0) + " x");
  } 
  mirrorYBox.oninput = function() {
    sendMsg("rtm" + (this.checked ? 1 : 0) + " y");
  } 
  
  function submit() {
    let ip = document.getElementById("ip_input").value;
    let fileName = idtfSelector.options[idtfSelector.selectedIndex].getAttribute("fileName");
    let scanSpeed = 30000;
    let fps = 30;
    let holdTime = 5;
  
    if (ip != null && fileName != null) {
      let request = {};
  
      request.ip = ip;
      request.fileName = fileName;
      request.scanSpeed = scanSpeed;
      request.fps = fps;
      request.holdTime = holdTime;
      request.serviceid = serviceID_input.value;
  
      post('/api/player', request)
        .catch(error => console.error(error));
  
      function post(url, data) {
        displayProgress();
        return fetch(url, {
          body: JSON.stringify(data),
          headers: {
            'content-type': 'application/json'
          },
          method: 'POST'
        })
          .then(response => interpretPlayerResponse(response.status));
      }
    }
  }
  
  function stopPlayer() {
    console.log("stopPlayer")
    query = fetch('/api/stopPlayer')
      .then(function (query) {
        if (query.ok === true) {
          hideProgess();
        } else {
          alert("Error stopping Player.");
        }
      })
  }
  
  function interpretPlayerResponse(code) {
    console.log(code);
    /*switch (code) {
      case 204:
      case 500:
      default:
        break;
    }*/
    hideProgess();
  }
  
  function sendMsg(value) {
    var msg = value;
        if (!msg) {
          return;
        }
        // send the message as an ordinary text
        connection.send(msg);
  }
  
  //Websocket
  window.WebSocket = window.WebSocket || window.MozWebSocket;
  let connection = new WebSocket('ws://'+location.host+'/test');
  
  connection.onopen = function () {
    // connection is opened and ready to use
  };
  
  connection.onerror = function (error) {
    // an error occurred when sending/receiving data
  };
  
  connection.onmessage = function (message) {
    // try to decode json (I assume that each message
    // from server is json)
    try {
      var json = JSON.parse(message.data);
    } catch (e) {
      console.log('This doesn\'t look like a valid JSON: ',
          message.data);
      return;
    }
    // handle incoming message
  };
  
  //https://medium.com/@martin.sikora/node-js-websocket-simple-chat-tutorial-2def3a841b61