// Buttons
const refreshButton = document.getElementById('refreshButton');
const scanButton = document.getElementById('scanButton');


// Button Event Listeners
scanButton.addEventListener('click', event => {
  displayProgress();
  query = fetch('/api/runServerlist')
    .then(function (query) {
      if (query.ok === true) {
        refreshIDNServerList();
      } else {
        alert("Error scanning IDN devices.");
      }
      hideProgess();
    })
});

refreshButton.addEventListener('click', event => {
  refreshIDNServerList();
});

function refreshIDNServerList() {
  response = fetch('/api/serverlist')
    .then(function (response) {
      return response.json();
    })
    .then(function (response) {
      let string = JSON.stringify(response);
      let parsed = JSON.parse(string);
      return (parsed);
    })
    .then(function (file) {
      let time = file.Time;

      let mainNode = document.getElementsByTagName("main");                       //DOM modification inside mainNode
      let containerNode = document.createElement("div");                          //encapsulate response in containerNode div

      //check if serverlist is already displayed in browser - then remove Node 
      let previousServerlistNode = document.getElementById("serverlist");
      if (previousServerlistNode !== null) {
        previousServerlistNode.parentNode.removeChild(previousServerlistNode);
      }
      
      containerNode.setAttribute("id", "serverlist");

      //"info" header of serverlist response
      let info = document.createElement("article");
        info.setAttribute("class", "info");
        let p = [document.createElement("p")];

        for (i = 0; i < 3; i++) {
         p.push(document.createElement("p"));
        }

        p[0].textContent = "Time: " + time;
        p[1].textContent = "SocketError: " + file.SocketError;
        p[2].textContent = "GetServerListError: " + file.GetServerlistError;
        p[3].textContent = "ServerCount: " + file.ServerList.length;

        for (i = 0; i < 4; i++) {
          info.appendChild(p[i]);
        }

      containerNode.appendChild(info);                                            //append "info" header to containerNode

      //handle serverlist IDN device responses
      if (file.ServerList.length > 0) {
        for (let i = 0; i < file.ServerList.length; i++) {
          let server = file.ServerList[i];

          let art = document.createElement("article");
            art.setAttribute("class", "server");
            let h2 = document.createElement("h2");
            h2.textContent = server.HostName;
            art.appendChild(h2);

          for (var key in server) {
            if (server.hasOwnProperty(key) && ((key != "HostName") && (key != "ProvidedServices"))) {
              let p = document.createElement("p");
              p.textContent = key + ": " + server[key];
              art.appendChild(p);
            }
          }

          containerNode.appendChild(art);

          let p = document.createElement("p");
            p.textContent = "Services:";
            art.appendChild(p);
          let services = file.ServerList[i].ProvidedServices;
            let ul = document.createElement("ul");
            if (services.length == 0) {
              let li = document.createElement("li");
                li.textContent = "No Services";
                ul.appendChild(li);
            } else {
              for (j = 0; j < services.length; j++) {
                let li = document.createElement("li");
                  li.textContent = " " + services[j].ServiceID + ". " + services[j].ServiceName;
                  ul.appendChild(li);
              }
            }
          art.appendChild(ul);
        }
      }
      _ = mainNode[0].insertBefore(containerNode, null);
    })
}