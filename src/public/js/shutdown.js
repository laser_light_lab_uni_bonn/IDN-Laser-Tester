let requestedShutdown = false;    //Flag: set on shutdown request

function shutdown() {

  let confirmShutdown = confirm("Power down server?");
  if (confirmShutdown == true) {

    if (requestedShutdown == true) {
      // is executed when shutdown request has already been send
      // -> may have failed. Most likely due to the server not being configured to run shutdown command as sudo without entering password.
      // hence showing warning and asking to try again
      let failedRequestWarning = confirm("It looks like you have already tried to shutdown?\n\nIf the server is still up and running, make sure you configured the shutdown permissions. For more information refer to the documentation's installation section.\n\nTry again?");
      if (failedRequestWarning) {
        shutdownRequest()
      }

    } else {
      shutdownRequest()
    }
  }

}

function shutdownRequest() {
  requestedShutdown = true;   // set flag
  query = fetch('/api/shutdownServer')
    .then(function (query) {
      if (query.ok === true) {
        alert(query.statusText);
      }
    })
}