// #### checkServiceInputEditability()
// Checks ServiceSelection 
// Sets ip_ and serviceID_input to readonly when selected "custom" vv.

let services = null;

function checkServiceInputEditability() {
  let ip_input = document.getElementById("ip_input");
  let serviceID_input = document.getElementById("serviceID_input");

  let serviceSelector = document.getElementById("serviceSelector");
  let selected = serviceSelector.options[serviceSelector.selectedIndex];

  if (selected.getAttribute('value') == "custom") {
    ip_input.removeAttribute("readonly");
    serviceID_input.removeAttribute("readonly");
  } else {
    ip_input.value = selected.getAttribute("ip");
    ip_input.setAttribute("readonly", "true");
    serviceID_input.value = selected.getAttribute("serviceid");
    serviceID_input.setAttribute("readonly", "true");
  }
}

// #### eventListener
// for updating IP & serviceID input fields Editability on selection change

document.getElementById("serviceSelector").addEventListener('change', event => {
  checkServiceInputEditability();
});

// #### 

function helloScan() {
  query = fetch('/api/runServerlist')
    .then(function (query) {
      if (query.ok === true) {
        refreshIDNServerList();
      } else {
        //alert("Error scanning IDN devices.");
        showToast('Error scanning IDN devices.', 3000, '#DB0000')
      }
    })
}

// #### refreshIDNServerList()
// queries serverList json from server (may be created onload)
// reads json file and creates options for service Selector

function refreshIDNServerList() {
  response = fetch('/api/serverlist')
    .then(function (response) {
      return response.json();
    })
    .then(function (response) {
      let string = JSON.stringify(response);
      let parsed = JSON.parse(string);
      return (parsed);
    })
    .then(function (file) {

      let scantime = document.getElementById("scantime");
      scantime.innerText = "Scan Time: " + file.Time;

      let serviceSelector = document.getElementById("serviceSelector");
      deleteChildNodes(serviceSelector);

      // handle serverlist IDN device responses
      if (file.ServerList.length > 0) {
        for (let i = 0; i < file.ServerList.length; i++) {
          let server = file.ServerList[i];

          if (server.ProvidedServices.length != 0) {
            for (let j = 0; j < server.ProvidedServices.length; j++) {
              let option = document.createElement("option");
              option.innerHTML = server.ProvidedServices[j].ServiceID + "-" + server.ProvidedServices[j].ServiceName + "@" + server.HostName;
              option.setAttribute("ip", server.ServerAddress);
              option.setAttribute("serviceID", server.ProvidedServices[j].ServiceID);
              option.setAttribute("value", "selection");
              serviceSelector.appendChild(option);
            }
          }
        }
      } else {
        let option = document.createElement("option");
        option.innerHTML = "- No Results -"
        serviceSelector.appendChild(option);
      }

      showToast(`Reading Servicelist successfull: ${file.ServerList.length} available.`);

      let option = document.createElement("option");
      option.setAttribute("value", "custom");
      option.innerHTML = "Custom";
      serviceSelector.appendChild(option);

      checkServiceInputEditability();
    })
}

// #### required by refreshIDTFFileList()
// #### getTypes()
// IDTF file list json file as input parameter
// reads json file and creates array of all used Types in json file

function getTypes(input) {
  let types = [];

  _ = JSON.parse(input, function (key, value) {
    if (key === "Type") {
      if (!(types.includes(value))) {
        types.push(value);
      }
    }
  })
  return types;
}

// #### refreshIDTFFileList()
// queries IDTF json from server
// reads json file and creates options for IDTF selector (sorted by type using optgroups)


function refreshIDTFFileList(calledBy) {
  let idtfSelector = document.getElementById("idtfSelector");
  let tester = calledBy ? calledBy : 0;

  response = fetch('/api/basicIDTFFiles')
    .then(function (response) {
      return response.json();
    })
    .then(function (response) {
      let string = JSON.stringify(response);
      let typeArray = getTypes(string);
      let parsed = JSON.parse(string);
      return ([parsed, typeArray]);
    })
    .then(function (passed) {

      deleteChildNodes(idtfSelector);

      let file = passed[0];
      let types = passed[1];

      if (types.length < 1) {
        showToast("IDTF List on server is empty. Please perform scan.", 2000, red_c);
      } else {
        for (let i = 0; i < types.length; i++) {
          let optgroup = document.createElement("optgroup");
          optgroup.setAttribute("label", types[i]);

          for (let j = 0; j < file.files.length; j++) {
            if (file.files[j].Type === types[i]) {
              // append all if not mdl-tester - if not, append all with Test property == 1
              if (tester === 0 || file.files[j].Tester === 1) {
                let option = document.createElement("option");
                option.setAttribute("class", "idtfOption");
                option.setAttribute("type", types[j]);
                option.setAttribute("fileName", file.files[j].FileName);
                option.setAttribute("thumbnail", file.files[j].Thumbnail);
                option.innerHTML = file.files[j].Name;

                optgroup.appendChild(option);
              }
            }
          }
          // append only non-empty optgroups
          if (optgroup.childElementCount > 0) {
            idtfSelector.appendChild(optgroup);
          }
        }
      }
    })
}


// ##########
// ## Play IDTF File - send Server Request
// ##########

// ##### required by sendMsg()
// #####
// WebSocket Setup for Realtime value transmission to webserver

function updateConectionStatus() {
  let icon = document.getElementById("connection-status-icon");
  let text = document.getElementById("connection-status-text");
  switch (realtimeWS.readyState) {
    case 0:
      icon.innerText = "sync";
      text.innerText = " opening..."
      break;
    case 1:
      icon.innerText = "cloud_done";
      text.innerText = " Ready"
      break;
    case 2:
      icon.innerText = "sync_problem";
      text.innerText = " closing..."
      break;
    case 3:
      icon.innerText = "sync_problem";
      text.innerText = " lost"
      break;
  }


}

window.WebSocket = window.WebSocket || window.MozWebSocket;
let realtimeWS = new WebSocket('ws://' + location.host + '/test');
updateConectionStatus()

realtimeWS.onopen = function () {
  updateConectionStatus()
  console.log("WebSocket is connected and ready.");
};

realtimeWS.onclose = function () {
  updateConectionStatus()
  console.log("WebSocket connection just closed.");
  alert("You lost realtime connection to the IDN-Laser-Tester server (Websocket closed). For RealTime Parameter modification, try to refresh the page.\nYou may have lost network connectivity or the webserver had to restart.")
};

realtimeWS.onmessage = function (message) {
  // try to decode json (I assume that each message
  // from server is json)
  try {
    var json = JSON.parse(message.data);
    if (json["type"] !== undefined) {
      if (json.type == "services") {
        services = json;
      }
    }
  } catch (e) {
    console.log('This doesn\'t look like a valid JSON: ',
      message.data);
    return;
  }
  // handle incoming message
  // not used since webserver is not yet responding to anything 
};
//https://medium.com/@martin.sikora/node-js-websocket-simple-chat-tutorial-2def3a841b61


// #####
// sendMsg()
// Sends Realtime values to webserver using a WebSocket

function sendMsg(value) {
  var msg = value;
  if (!msg) {
    return;
  }
  // send the message as an ordinary text
  realtimeWS.send(msg);
}

// #####
// Realtime Slider Event listeners
// Send RealTime Values

document.getElementById("scaleSlider").oninput = function () {
  //scaleValueLabel.innerHTML = this.value + " %";
  sendMsg("rtm" + this.value + " s");
}
document.getElementById("rSlider").oninput = function () {
  //rValueLabel.innerHTML = this.value + " %";
  sendMsg("rtm" + this.value + " r");
}
document.getElementById("gSlider").oninput = function () {
  //gValueLabel.innerHTML = this.value + " %";
  sendMsg("rtm" + this.value + " g");
}
document.getElementById("bSlider").oninput = function () {
  //bValueLabel.innerHTML = this.value + " %";
  sendMsg("rtm" + this.value + " b");
}
document.getElementById("sftSlider").oninput = function () {
  sendMsg("rtm" + (this.value) + " c");
}
document.getElementById("mirrorXBox").oninput = function () {
  sendMsg("rtm" + (this.checked ? -1 : 1) + " x");
}
document.getElementById("mirrorYBox").oninput = function () {
  sendMsg("rtm" + (this.checked ? -1 : 1) + " y");
}

document.getElementById("flashBOButton").onmouseup = function () {
  flashBO();
}

function flashBO() {

  setTimeout(() => {
    for (i = 0; i < 5; i++) {
      sendMsg("rtm" + document.getElementById("rSlider").value + " r");
      sendMsg("rtm" + document.getElementById("gSlider").value + " g");
      sendMsg("rtm" + document.getElementById("bSlider").value + " b");
      //console.log("restore " + i);
    }
  }, 200);
  for (i = 0; i < 5; i++) {
    sendMsg("rtm" + 0 + " r");
    sendMsg("rtm" + 0 + " g");
    sendMsg("rtm" + 0 + " b");
    //console.log("flash BO " + i);
  }
}

// #### required by startPlayer()
// #### checkFormValidity()
// Checks whether all inputs .needs-validation contain valid data
// otherwise return 1 and show Validity report on input

function checkFormValidity() {
  let error = 0;
  array = document.getElementsByClassName("needs-validation");
  for (i = 0; i < array.length; i++) {
    if (!array[i].validity.valid) {
      array[i].reportValidity();
      error = 1;
    }
  }
  return error;
}

// #####
// stopPlayer()
// Sends Stop Request - server will cancel idn-C-idtfPlayer

function stopPlayer() {
  console.log(now() + ": requested single stop");
  query = fetch('/api/stopPlayer')
    .then(function (query) {
      if (query.ok === true) {
        hideMDLProgressInterminate();
        showToast(query.statusText, 2000)
      } else {
        showToast(query.statusText, 3000);
      }
    })
}

function stopAll() {
  console.log(now() + ": requested stop for all active")
  query = fetch('/api/stopAllPlayers')
    .then(function (query) {
      if (query.status == 200) {
        hideMDLProgressInterminate();
        //alert(query.statusText);
        showToast(query.statusText, 3000)
      } else {
        hideMDLProgressInterminate();
        // alert(query.statusText);
      }
    })
}

// #####
// MDL Card Menu Actions
// #####

// ##### refreshDestinationCard()
// Scans for servers and updates Card

function refreshDestinationCard() {
  helloScan();
  refreshIDNServerList();
}

// #####
// interpretPlayerResponse()
// Different server responses for cancellation or normal termination after reaching end of file 

function interpretPlayerResponse(response) {
  showToast(response.statusText, 2000);
  hideMDLProgressInterminate();
}