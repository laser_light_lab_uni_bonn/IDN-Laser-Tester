let scaleSlider = document.getElementById("scaleSlider");
let rSlider = document.getElementById("rSlider");
let gSlider = document.getElementById("gSlider");
let bSlider = document.getElementById("bSlider");
let mirrorXBox = document.getElementById("mirrorXBox");
let mirrorYBox = document.getElementById("mirrorYBox");

let scaleValueLabel = document.getElementById("scaleValueLabel");
let rValueLabel = document.getElementById("rValueLabel");
let gValueLabel = document.getElementById("gValueLabel");
let bValueLabel = document.getElementById("bValueLabel");
//let stopButton = document.getElementById('stopPlayerButton');

scaleValueLabel.innerHTML = scaleSlider.value + " %";
rValueLabel.innerHTML = rSlider.value + " %";
gValueLabel.innerHTML = gSlider.value + " %";
bValueLabel.innerHTML = bSlider.value + " %";

// Update the current slider value (each time you drag the slider handle)
scaleSlider.oninput = function () {
  scaleValueLabel.innerHTML = this.value + " %";
  sendMsg("rtm" + this.value + " s");
}
rSlider.oninput = function () {
  rValueLabel.innerHTML = this.value + " %";
  sendMsg("rtm" + this.value + " r");
}
gSlider.oninput = function () {
  gValueLabel.innerHTML = this.value + " %";
  sendMsg("rtm" + this.value + " g");
}
bSlider.oninput = function () {
  bValueLabel.innerHTML = this.value + " %";
  sendMsg("rtm" + this.value + " b");
}
mirrorXBox.oninput = function () {
  sendMsg("rtm" + (this.checked ? 1 : 0) + " x");
}
mirrorYBox.oninput = function () {
  sendMsg("rtm" + (this.checked ? 1 : 0) + " y");
}

function submit() {
  let ip = document.getElementById("ip_input").value;
  let fileName = document.getElementById("idtfFile_input").value;
  let scanSpeed = document.getElementById("scanRate_input").value;
  let fps = document.getElementById("fps_input").value;
  let holdTime = document.getElementById("hold_input").value;

  console.log("IP: " + ip + " File: " + fileName);

  if (ip != null && fileName != null) {
    let request = {};

    request.ip = ip;
    request.fileName = fileName;
    request.scanSpeed = scanSpeed;
    request.fps = fps;
    request.holdTime = holdTime;

    post('/api/player', request)
      .catch(error => console.error(error));

    function post(url, data) {
      displayProgress();
      return fetch(url, {
        body: JSON.stringify(data),
        headers: {
          'content-type': 'application/json'
        },
        method: 'POST'
      })
        .then(response => interpretPlayerResponse(response.status));
    }
  }
}

function stopPlayer() {
  console.log("stopPlayer")
  query = fetch('/api/stopPlayer')
    .then(function (query) {
      if (query.ok === true) {
        hideProgess();
      } else {
        alert("Error stopping Player.");
      }
    })
}

function interpretPlayerResponse(code) {
  console.log(code);
  /*switch (code) {
    case 204:
    case 500:
    default:
      break;
  }*/
  hideProgess();
}

function sendMsg(value) {
  var msg = value;
  if (!msg) {
    return;
  }
  // send the message as an ordinary text
  connection.send(msg);
}

//Websocket
window.WebSocket = window.WebSocket || window.MozWebSocket;
let connection = new WebSocket('ws://' + location.host + '/test');

connection.onopen = function () {
  // connection is opened and ready to use
};

connection.onerror = function (error) {
  // an error occurred when sending/receiving data
};

connection.onmessage = function (message) {
  // try to decode json (I assume that each message
  // from server is json)
  try {
    var json = JSON.parse(message.data);
  } catch (e) {
    console.log('This doesn\'t look like a valid JSON: ',
      message.data);
    return;
  }
  // handle incoming message
};

//https://medium.com/@martin.sikora/node-js-websocket-simple-chat-tutorial-2def3a841b61