function displayMDLProgressInterminate(elem_id) {
    let id = "mdl-interminate-progress";
    if (elem_id) id = elem_id;
    let progress = document.getElementById(id);
    progress.removeAttribute('hidden');
}

function hideMDLProgressInterminate(elem_id) {
    let id = "mdl-interminate-progress";
    if (elem_id) id = elem_id;
    let progress = document.getElementById(id);
    progress.setAttribute('hidden', true);
}

//TODO: Animation