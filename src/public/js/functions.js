//predefined colors
let red_c = '#DB0000';

function deleteChildNodes(node) {
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
}

function showToast(message, timeout, color) {

    'use strict';
    window['counter'] = 0;
    var snackbarContainer = document.getElementById('demo-toast-example');
    if (color != null) {
        snackbarContainer.style.backgroundColor = color;
    }
    var showToastButton = document.getElementById('demo-show-toast');
    var data = { message: message, timeout: timeout };
    snackbarContainer.MaterialSnackbar.showSnackbar(data);

}

function now() {
    return Date().slice(4, 24);
}