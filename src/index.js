// ######################################################
//      ________  _   __   ______          __            
//     /  _/ __ \/ | / /  /_  __/__  _____/ /____  _____ 
//     / // / / /  |/ /    / / / _ \/ ___/ __/ _ \/ ___/ 
//   _/ // /_/ / /|  /    / / /  __(__  ) /_/  __/ /     
//  /___/_____/_/ |_/    /_/  \___/____/\__/\___/_/      
//
// ######################################################



// Node webserver
// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


// ## used modules

const express = require("express"); // Webserver
const fs = require("fs-extra"); // File acces       
const path = require("path");
const { spawn } = require('child_process'); // Execute program files on server
const exec = require('child_process').exec; // Run bash scripts
const bodyParser = require('body-parser'); // encode
var dgram = require('dgram'); // UDP support
var multer = require('multer');


// ## global variables

let player; // enables second api to stop player at any time
let services = {};
services.type = "services";
services.services = [];
let activePlayers = [];
let websocket = null;
// + see DGRAM / UDP Setup











// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

// ## Perform on startup

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

// parse serverList if List exists. If server needs to restart you would otherwise have to perform a scan first even if nothing changed.

parseServices();

if (!(ifFileExists('./scripts/runServerList.sh') && ifFileExists('./idn-C-serverList/bin-linux/serverList'))) {
    console.log("ERROR: IDN software missing.")
    if (ifFileExists('compile.sh')) {
        console.log("  -> found compile script, executing")
        compile = exec("sh compile.sh");

        compile.on("close", function(code) {
            console.log(`ServerList: child process exited with code ${code}`);
            if (ifFileExists('./scripts/runServerList.sh') && ifFileExists('./idn-C-serverList/bin-linux/serverList')) {
                console.log("  -> success!\n  -> proceeding...\n");
            }
        });
    }
}











//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//     EXPRESS Web Server

//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const expressWs = require('express-ws')(app); // init express WebSocket

// starts webserver
app.listen(3000, "0.0.0.0", () => console.log('IDN Test App listening on port 3000.\nYou may want to configure an iptables roule to forward port 80 to 3000 without the need for running nodejs as root'));

// Redirecting:

// redirect for node_module dependencies

app.get('/node_modules/*', function(req, res) {
    res.sendFile(path.join(path.resolve(), req.url));
});

// redirect idtfThumbnail requests
// checks existance - or returns noPreview image
app.get("/idtfThumbnails/*", function(req, res) {
    let url = "/public/";
    if (fs.existsSync("public" + req.url)) {
        url += req.url;
    } else {
        url += "idtfThumbnails/noPreview.png";
    }
    res.sendFile(path.join(path.resolve(), url));
});

// -> see API Section











//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//     Functions

//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx



function parseServices() {
    file = parseJSONFile('./public/json/serverList.json');

    if (file != null && file.ServerList.length > 0) {
        for (let i = 0; i < file.ServerList.length; i++) {
            let server = file.ServerList[i];

            if (server.ProvidedServices.length != 0) {
                for (let j = 0; j < server.ProvidedServices.length; j++) {
                    let service = {};
                    service.ServiceName = server.ProvidedServices[j].ServiceName;
                    service.ServiceID = server.ProvidedServices[j].ServiceID;
                    service.HostName = server.HostName;
                    service.ServerAddress = server.ServerAddress;
                    service.player = null;
                    services.services.push(service)
                }
            }
        }
        updateClientsServices();
    }
}

function parseJSONFile(path) {
    try {
        if (ifFileExists(path)) {
            return JSON.parse(fs.readFileSync(path));
        }
    } catch (err) {
        console.log("ERROR while trying to open file ", path);
    }
}

function ifFileExists(path) {
    try {
        if (fs.existsSync(path)) {
            return true;
        } else {
            return false;
        }
    } catch (err) {
        console.log("ERROR while trying to open file ", path);
    }
}

function httpRespond(res, code, message) {
    console.log(" -> Browser: ", message)
    res.statusMessage = message;
    res.statusCode = code;
    res.end();
    //res.status(code).end();
}

function updateClientsServices() {
    //websocket.send(JSON.stringify(services));
    //TODO
}

function updateJSONFile(res, insert) {
    let fileName = './public/json/basicIDTF.json';
    let key = 'files';

    let file = parseJSONFile(fileName);
    if (file == undefined) {
        httpRespond(res, 400, "Error while reading " + fileName);
        return;
    }

    if (!checkIfEntryExistsInArray(file[key], 'FileName', insert.FileName)) {
        file[key].push(insert);
    } else {
        httpRespond(res, 400, "This file is already in the list.");
        return;
    }

    fs.writeFile(fileName, JSON.stringify(file, null, 2), function(err) {
        if (err) {
            httpRespond(res, 400, "Error while updating " + fileName);
            return console.log(err);
        }
        console.log('writing to ' + fileName);
        httpRespond(res, 200, "Server: Upload successfull!");
    });
}

function checkIfEntryExistsInArray(array, key, value) {
    console.log(array, key, value);
    for (i = 0; i < array.length; i++) {
        if (array[i][key] == value) {
            return true;
        }
    }
    return false;
}









//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//     APIs

//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


app.get('/api/serverlist', (req, res) => {

    // Return the serverList.json file

    list = parseJSONFile('./public/json/serverList.json');
    if (list != null) {
        res.send(list);
    }
});

app.get('/api/basicIDTFFiles', (req, res) => {

    // Return the basicIDTF file (listing all IDTF files)

    let list = parseJSONFile('./public/json/basicIDTF.json');
    res.send(list);
});

app.get('/api/runServerlist', (req, res) => {

    // Start IDN Network / Hello scan (incl. service discovery) and save Responses to serverList.json

    if (ifFileExists('./scripts/runServerList.sh') && ifFileExists('./idn-C-serverList/bin-linux/serverList')) {
        const serverListHandler = exec("sh ./scripts/runServerList.sh");
        parseServices();

        serverListHandler.stdout.on("data", function(data) {
            console.log(data);
        });

        serverListHandler.stderr.on("data", function(data) {
            console.log(data);
        });

        serverListHandler.on("close", function(code) {
            console.log(`ServerList: child process exited with code ${code}`);
            if (code === 0) {
                res.sendStatus(200);
            } else {
                res.sendStatus(500);
            }
        });
    } else {
        console.log("ERROR: ServerList or start script do not exist. Can't perform scan.");
        res.sendStatus(500);
    }
});

app.post('/api/player', function(req, res) {

    // Starts an idtf-C-player instance using the given parameters

    if (req.body.ip == null) {
        console.log("ERROR: IDTF Player: No valid IP Address");
        httpRespond(res, 400, "No valid IP Address");
    } else {

        let parameter = Object.entries(req.body);
        let params = [];

        let ip;
        let serviceid = null;

        for (let i = 0; i < parameter.length; i++) {
            switch (parameter[i][0]) {
                case "ip":
                    params.push("-hs");
                    params.push(parameter[i][1]);
                    ip = parameter[i][1];
                    break;
                case "fileName":
                    params.push("-idtf");
                    let file = "./public/idtf/" + parameter[i][1];
                    if (ifFileExists(file)) {
                        params.push("./public/idtf/" + parameter[i][1]);
                    } else {
                        httpRespond(res, 400, "The requested IDTF file does not exist");
                        return;
                    }
                    break;
                case "scanSpeed":
                    params.push("-pps");
                    params.push(parameter[i][1]);
                    break;
                case "fps":
                    params.push("-fr");
                    params.push(parameter[i][1]);
                    break;
                case "holdTime":
                    params.push("-hold");
                    params.push(parameter[i][1]);
                    break;
                case "mx":
                    params.push("-mx");
                    // default expects flag + value. But mirroring is flag only
                    break;
                case "my":
                    params.push("-my");
                    // default expects flag + value. But mirroring is flag only
                    break;
                case "serviceid":
                    params.push("-serviceid");
                    params.push(parameter[i][1]);
                    serviceid = parameter[i][1];
                    serviceid = ((serviceid == "") ? 0 : serviceid)
                    break;
                default:
                    params.push("-" + parameter[i][0]);
                    params.push(parameter[i][1]);
                    break;
            }
        }

        let index = services.services.findIndex(x => x.ServerAddress == ip && x.ServiceID == serviceid);

        if (index == -1) {
            console.log("ERROR: Internal Service List is empty. Perfrom scan.");

            let service = {};
            service.ServiceName = "Custom";
            service.ServiceID = ((serviceid == "") ? 0 : serviceid);
            service.ServerAddress = ip;
            service.player = null;
            services.services.push(service)

            services.services.push

            index = services.services.findIndex(x => x.ServerAddress == ip && x.ServiceID == serviceid);
        }
        if (services.services[index].player == null) {
            if (ifFileExists("./idn-C-idtfPlayer/bin-linux/idtfPlayer")) {
                services.services[index].player = spawn("./idn-C-idtfPlayer/bin-linux/idtfPlayer", params);
                activePlayers.push(index);

                // Add player event listeners

                services.services[index].player.stdout.on("data", function(data) {
                    console.log(data.toString('utf8'));
                });

                services.services[index].player.stderr.on("data", function(data) {
                    console.log(data);
                });

                services.services[index].player.on("close", function(code) {
                    console.log(`IDTF: child process exited with code ${code}`);
                    updateClientsServices();
                    httpRespond(res, 200, `Server: Player ${services.services[index].ServiceName}(#${serviceid})@${ip} stopped.`);
                    services.services[index].player = null;
                    if (activePlayers.indexOf(index) != -1) {
                        // remove player from list of active Players if process exits normally
                        activePlayers.splice(activePlayers.indexOf(index), 1);
                    }
                });
            } else {
                console.log("ERROR: idn-C-idtfPlayer not found.")
            }
        } else {
            message = `Service ${services.services[index].ServiceName}(#${serviceid})@${ip} is currently busy`;
            console.log(message);
            httpRespond(res, 500, message);
        }
    }
});

app.get('/api/stopPlayer', (req, res) => {

    // stop the most recent player instance

    if (activePlayers.length > 0) {
        console.log("stopPlayer");
        index = activePlayers.pop();
        services.services[index].player.stdin.pause();
        services.services[index].player.kill();
        services.services[index].player = null;

        httpRespond(res, 200, `Stopped Player ${services.services[index].ServiceName}(#${services.services[index].ServiceID})@${services.services[index].ServerAddress}`);
    } else {
        httpRespond(res, 500, "There are currently no active players, hence no to stop.");
    }
});

app.get('/api/stopAllPlayers', (req, res) => {

    // Emergency Stop: stops all player instances at once

    if (activePlayers.length > 0) {
        let count = activePlayers.length;
        for (i = 0; i < count; i++) {
            index = activePlayers.pop();
            console.log(`USER: stopping Player ${services.services[index].ServiceName}(#${services.services[index].ServiceID})`);
            services.services[index].player.stdin.pause();
            services.services[index].player.kill();
            services.services[index].player = null;
        }
        httpRespond(res, 200, `Stopping ${count} players`);
    } else {
        httpRespond(res, 200, "There are currently no active players, hence no to stop.");
    }
});

app.get('/api/shutdownServer', (req, res) => {

    // Power down server

    if (activePlayers.length > 0) {
        let count = activePlayers.length;
        for (i = 0; i < count; i++) {
            index = activePlayers.pop();
            console.log(`USER: stopping Player ${services.services[index].ServiceName}(#${services.services[index].ServiceID})`);
            services.services[index].player.stdin.pause();
            services.services[index].player.kill();
            services.services[index].player = null;
        }
        httpRespond(res, 200, `Stopped ${count} running players. Shutting down host`);
        exec('sudo /sbin/shutdown now');
    } else {
        httpRespond(res, 200, "Shutting down host.");
        exec('sudo /sbin/shutdown now');
    }

});


// ##
// #### Receiving / handling / storing (IDTF) files from the client

var storage = multer.diskStorage({

    // defines where to save received files

    destination: function(req, file, cb) {
        cb(null, './public/uploads/');
    },
    filename: function(req, file, cb) {
        if (file.fieldname == 'idtf') {
            cb(null, req.body.idtfFileName);
        } else if (file.fieldname == 'thumbnail') {
            cb(null, req.body.thumbnailFileName);
        }
    }
});
var upload = multer({ storage: storage });

app.post('/api/upload', upload.fields([{

    // actual save request

    name: 'idtf',
    maxCount: 1
}, {
    name: 'thumbnail',
    maxCount: 1
}]), function(req, res, next) {

    let idtfFileName = null;
    let thumbnailFileName = null;

    if (ifFileExists('./public/uploads/' + req.body.idtfFileName)) {
        idtfFileName = req.body.idtfFileName;
    } else {
        return;
    }
    if (ifFileExists('./public/uploads/' + req.body.thumbnailFileName)) {
        thumbnailFileName = req.body.thumbnailFileName;
    }

    entry = {};
    if (req.body.dislayName != '') {
        entry.Name = req.body.displayName;
    }
    if (idtfFileName == '') {
        return;
    }
    fs.renameSync('./public/uploads/' + idtfFileName, './public/idtf/' + req.body.type + '/' + idtfFileName);
    entry.FileName = req.body.type + '/' + idtfFileName;
    if (thumbnailFileName != '' && thumbnailFileName != null) {
        fs.renameSync('./public/uploads/' + thumbnailFileName, './public/idtfThumbnails/' + thumbnailFileName);
        entry.Thumbnail = thumbnailFileName;
    }
    entry.Type = req.body.type;
    updateJSONFile(res, entry);

})

app.post('/api/deleteIDTF', function(req, res) {

    // handles deletion request for IDTF files using the file path as identification

    if (req.body.idtfFileName == null || req.body.idtfFileName == '') {
        console.log("ERROR: Empty Deletion Request.");
        httpRespond(res, 400, "Server: Got empty deletion request");
        return;
    } else {
        let fileName = req.body.idtfFileName.replace('..', '');

        let file = parseJSONFile('./public/json/basicIDTF.json');
        if (file == undefined) {
            httpRespond(res, 400, "Error reading IDTF List");
            return;
        }

        let flag = 0;

        for (i = 0; i < file.files.length; i++) {
            if (file.files[i].FileName == fileName) {
                flag++;
                file.files.splice(i, 1);
                break;
            }
        }

        if (flag > 0) {
            fs.writeFile('./public/json/basicIDTF.json', JSON.stringify(file, null, 2), function(err) {
                if (err) {
                    httpRespond(res, 400, "Error while updating IDTF List");
                    return console.log(err);
                }
                console.log('writing to ./public/json/basicIDTF.json');

                if (ifFileExists('./public/idtf/' + fileName)) {
                    console.log("IDTF Deletion Request: File exists.");

                    fs.unlink('./public/idtf/' + fileName, function(err) {
                        if (err) {
                            console.log(err);
                            httpRespond(res, 400, "Server: Removed from list only. Error deleting IDTF file!");
                        };
                        console.log('File deleted: ' + fileName);
                    });
                } else {
                    httpRespond(res, 400, "Server: Removed from list only. IDTF file did not exist!");
                }


                httpRespond(res, 200, "Server: Deletion successfull!");
            });
        }

    }
});












//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//     Web Socket API (Realtime Communucation)

//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

app.ws('/test', function(ws, req) {

    websocket = ws;

    ws.on('message', function(msg) {
        if (msg.indexOf('rtm') == 0) {
            sendUDPMessage(msg);
        } else {
            //
        }
    });
});

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

// redirects everything else to public folder
// has to be the last one so that the other APIs are not affected

app.get('/*', function(req, res) {
    const dest = '/public' + req.url;
    //console.log("Route: " + dest);
    res.sendFile(path.join(path.resolve(), dest));
});












// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//     UDP Setup : Forwarding the RealTime Messages to the Player

//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

let PORT = 1234;
let HOST = '127.0.0.1';
let client = dgram.createSocket('udp4');

function sendUDPMessage(message) {
    client.send(message, 0, message.length, PORT, HOST, function(err, bytes) {
        if (err) throw err;
        //console.log('UDP message sent to ' + HOST +':'+ PORT);
    });
}










//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//     NodeJS Signal Handler

//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

process.on('SIGINT', function() {
    console.log("SIGINT")
    client.close();
    player.stdin.pause();
    player.kill();
    process.exit();
});