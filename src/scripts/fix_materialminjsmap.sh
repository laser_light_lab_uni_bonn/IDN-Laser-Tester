#!/bin/bash

cwd=${PWD##*/}

if ! [ "$cwd" = "scripts" ]; then
	echo "please run this script from within its parent directory (scripts) in order to work properly as it containts relative paths"
else 

	FILE=../node_modules/material-design-lite/material.min.js.map

	if ! [ -f "$FILE" ]; then
		echo "material.min.js.map does not exist at expected location"
		echo "checking relative ./dist directory"

		FILEalt=../node_modules/material-design-lite/dist/material.min.js.map

		if [ -f "$FILEalt" ]; then
			echo "found .map in suggested directory. copying..."
			cp $FILEalt ../node_modules/material-design-lite/
			echo "done!"
		fi

		echo "verifying..."

		if [ -f "$FILE" ]; then
					echo "success!"
		else
			echo "failed"
			fi 
	fi

fi
