#!/usr/bin/env bash

# Varaibles

previouseIP=127.0.0.1
wIP=$(ip -4 a show wlan0 | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/[0-9]{1,2}')
HN=$(hostname)
text=""
repositoryPath="/opt/IDN-Laser-Tester/"
SSIDerr=0

# Functions

updateSSID () {

  FILE=$repositoryPath\src/scripts/updateSSID.sh
  if [ ! -f "$FILE" ]; then
    clear
    printTopLogo
    printHeadline " INFORMATION"

    echo '  ! Updating the SSID failed !'
    echo "    The updateSSID.sh file could not be located"
    echo '    Please check the repositioryPath variable in the startupRaspi.sh file'

    SSIDerr=1
    
    sleep 10
  else 
    SSIDerr=0
    sudo $FILE $1
  fi
}

printFullscreenLogo () {
  clear
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo '  ############################################################################  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  #              ________  _   __   ______          __                       #  '                         
  echo '  #             /  _/ __ \/ | / /  /_  __/__  _____/ /____  _____            #  '
  echo '  #             / // / / /  |/ /    / / / _ \/ ___/ __/ _ \/ ___/            #  '
  echo '  #           _/ // /_/ / /|  /    / / /  __(__  ) /_/  __/ /                #  '
  echo '  #          /___/_____/_/ |_/    /_/  \___/____/\__/\___/_/                 #  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  ############################################################################  '
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
}

printTopLogo () {
  clear
  date
  echo '################################################################################'
  echo '                 ________  _   __   ______          __            '                         
  echo '                /  _/ __ \/ | / /  /_  __/__  _____/ /____  _____ '
  echo '                / // / / /  |/ /    / / / _ \/ ___/ __/ _ \/ ___/ '
  echo '              _/ // /_/ / /|  /    / / /  __(__  ) /_/  __/ /     '
  echo '             /___/_____/_/ |_/    /_/  \___/____/\__/\___/_/      '
  echo '                                                                  '
  echo '################################################################################'
  echo ''
}

printHeadline () {
  echo ''
  echo " - $1 - "
  echo ''
}

# 1. Show initial Splashscreen

printFullscreenLogo
sleep 2

# 2. Start Loop

while [ 1 ]
do

# get current Ethernet IP
currenteIP=$(ip -4 a show eth0 | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/[0-9]{1,2}')

# get current wireless IP
wIP=$(ip -4 a show wlan0 | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/[0-9]{1,2}')

# if new Ethernet IP > 0 and has changed:
if [ -n $currenteIP ] && [ ! $previouseIP = $currenteIP ]
then
  previouseIP=$currenteIP

  printTopLogo
  printHeadline " INFORMATION"

  echo '  The Ethernet IP has changed'
  echo "  It is now $previouseIP"
  echo '  Updating WiFi SSID...'

  updateSSID $currenteIP

  sleep 5
fi

printTopLogo
printHeadline "INSTRUCTIONS"

echo '    To use the IDN-Tester'
echo '    - connect your device with the same network as this server'
echo '    - open a webbrowser'
echo '    - type: '
echo '       -> http://'$HN' (perviously http://'$HN'.local)'
echo '          on your iPad, iPhone, Mac or Windows PC'
echo '       -> http://'$(echo $currenteIP | sed 's/\/.*//')'/'
echo '          on android phones, other devices or when the above doesn t work'
echo '       -> http://'$(echo $wIP |sed 's/\/.*//')'/'
echo '          when connected with the Raspi`s WiFi (pw testtest).'

if [ "$SSIDerr" -eq 1 ]; then
    echo " "
    echo "    !ERROR updating SSID. Check path in src/updateSSID.sh!"
fi

echo ''
pm2 ls

sleep 10

done
