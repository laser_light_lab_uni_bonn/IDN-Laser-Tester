#!/usr/bin/env bash

# Varaibles

previouseIP=127.0.0.1
HN=$(hostname)
text=""

# Functions

printFullscreenLogo () {
  clear
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo '  ############################################################################  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  #              ________  _   __   ______          __                       #  '                         
  echo '  #             /  _/ __ \/ | / /  /_  __/__  _____/ /____  _____            #  '
  echo '  #             / // / / /  |/ /    / / / _ \/ ___/ __/ _ \/ ___/            #  '
  echo '  #           _/ // /_/ / /|  /    / / /  __(__  ) /_/  __/ /                #  '
  echo '  #          /___/_____/_/ |_/    /_/  \___/____/\__/\___/_/                 #  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  #                                                                          #  '
  echo '  ############################################################################  '
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
  echo ''
}

printTopLogo () {
  clear
  date
  echo '################################################################################'
  echo '                 ________  _   __   ______          __            '                         
  echo '                /  _/ __ \/ | / /  /_  __/__  _____/ /____  _____ '
  echo '                / // / / /  |/ /    / / / _ \/ ___/ __/ _ \/ ___/ '
  echo '              _/ // /_/ / /|  /    / / /  __(__  ) /_/  __/ /     '
  echo '             /___/_____/_/ |_/    /_/  \___/____/\__/\___/_/      '
  echo '                                                                  '
  echo '################################################################################'
  echo ''
}

printHeadline () {
  echo ''
  echo " - $1 - "
  echo ''
}

# 1. Show initial Splashscreen

printFullscreenLogo
sleep 2

# 2. Start Loop

while [ 1 ]
do

# get current Ethernet IP
currenteIP=$(ip -4 a show enp0s3 | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/[0-9]{1,2}')

# if new Ethernet IP > 0 and has changed:
if [ -n $currenteIP ] && [ ! $previouseIP = $currenteIP ]
then
  previouseIP=$currenteIP

  printTopLogo
  printHeadline " INFORMATION"

  echo "  The Ethernet IP has changed to $previouseIP"

  sleep 5
fi

printTopLogo
printHeadline "INSTRUCTIONS"

echo '    To use the IDN-Tester'
echo '    - connect your device with the same network as this server'
echo '    - open a webbrowser'
echo '    - type: '
echo '       -> http://'$HN' (perviously http://'$HN'.local'
echo '          on your iPad, iPhone, Mac or Windows PC'
echo '       -> http://'$(echo $currenteIP | sed 's/\/.*//')'/'
echo '          on android phones, other devices or when the above doesn t work'
echo ''
pm2 ls

sleep 10

done
