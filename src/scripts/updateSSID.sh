#!/usr/bin/env bash

if [ -n $1 ]
then
  echo "called updateSSID.sh"

  sudo sed -i.bak "/ssid=/c\ssid=IDN@$1" /etc/hostapd/hostapd.conf

  sudo systemctl unmask hostapd
  sudo systemctl restart hostapd
fi
