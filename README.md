# IDN-Laser-Tester

Public repository for an IDN-based laser projector tester framework (ILDA Digital Network).

## Features

The IDN-Laser-Tester enables you to easily test your IDN enabled hardware (adapter or projectors) from anywhere at the event location you want, just using your smartphone, tablet or laptop.

 - For a detailed feature overview see [Features](/Documentation/Features.md) <br>
 - For more background information see [Background](/Documentation/Background.md)

Click on the thumbnail(s) below to go to a **quick demo video** on the IDN-Laser-Tester on **YouTube**:

<kbd>
<a href="https://www.youtube.com/watch?v=VNK9XVIt2ig" target="_blank">
<img src="/Documentation/sources/images/thumbnail-idn-laser-tester-demo.png" alt="Quick demo video (YouTube)" width="400" height="225" border="10" />
</a>
</kbd>
<kbd>
<a href="https://www.youtube.com/watch?v=9IPzKbKc47s" target="_blank">
<img src="/Documentation/sources/images/YouTube-ildalasershows_IDN-1st-Tech.png" alt="Quick demo video (YouTube)" width="400" height="225" border="10" />
</a>
</kbd>

(left thumbnail goes to the YouTube channel of "Laser & Light Lab Uni Bonn", right goes to the YouTube channel of ILDA / "ildalasershows")

### Use cases

- Test your setup right after construction: Before building up your workspace
  - Test whether all consumers are up, running and detectable in the network
  - Test whether scanner and laser emmiters are working correctly
  - Test whether everything works as expected before exacerbating physical access due to further construction
- Test your setup without the need for expensive Lasershow Sotware
- Troubleshoot your setup
  - Easily carry the tester around while walking accross the event location
  - Easily connect the tester to the IDN network at different positions to test wiring and routing

### Requirements

The IDN-Laser-Tester doesn't require you to install any software on your mobile device. Instead, you need to run a server component (a piece of software) somewhere in your network which can be easily accessed and controled from your mobile device's webbrowser. This also means, that the mobile device can have virtually any OS.

Basically, you need the following:

- IDN capable hardware or software
- A device connected to your local IDN network running a webbrowser
- A computer to run the IDN-Laser-Tester server component which provides the website accessed from the mobile device to control the tester.

For the exact requirements see the [Setup Guide](/Documentation/SetupGuide.md)

> **No IDN compatible hardware?** <br> If you do not own any IDN compatible hardware you can run a software which is capable of receiving IDN streams and displays the image on screen. The software is available for linux and Windows and can be downloaded [here](https://gitlab.com/laser_light_lab_uni_bonn/idn-npp/idn-tools/idn-toolbox). <br><br> *Mind that the "toolbox" is under development and may not be perfect.*<br><br> If the tool won't start just try again.

## Getting started

For more information and detailed instructions on how to install and run the IDN-Laser-Tester see the [Setup Guide](/Documentation/SetupGuide.md)

## Evaluation

If you are curious and want to test or improve the IDN-Laser-Tester, feel free to download and test the preview. See the [Getting Started](#prospects) section above to learn how to get and set the IDN Tester up.

> **Introduction** <br> For information about the IDN-LaserTester's capabilities see the [Features Page](/Documentation/Features.md). It can also be used as a step-by-step guide to get familiar with the Application as it guides through all features and has a few ToDos and tips after every section.

Furthermore, we would be pleased to receive some constructive feedback, filling in and submitting the [Evaluation (PDF)](https://uni-bonn.sciebo.de/s/2JvEQzxw45WEZEe). You will find all needed information to take part on the first page of the PDF.

## Acknowledgements

The IDN-Laser-Tester heavily depends on
- [idn-C-idtfPlayer](https://github.com/DexLogic/idn-C-idtfPlayer) by [DexLogic](https://github.com/DexLogic)
- [idn-C-serverList](https://github.com/DexLogic/idn-C-serverList) by [DexLogic](https://github.com/DexLogic)

and makes use of
- [NodeJS](https://nodejs.org/en/)
- [Express](https://expressjs.com/de/)
- [Material Design Lite](https://getmdl.io) by [Google](https://github.com/google)

The IDN-Laser-Tester currently includes a few IDTF files from the following repositories
- [Arroquw](https://github.com/Arroquw/Project-Laser)
- [jheidel](https://github.com/jheidel/ece4760-lab5/tree/master/datafiles)

The respecting references can be found in the `src/public/json/*.json` file and are displayed in the IDTF section of the web interface.
